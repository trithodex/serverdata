// TMW2/LoF Script.
// Author:
//    Jesusalva
// Notes:
//    Based on BenB idea.

017-2,45,22,0	script	Vault#0172	NPC_VAULT,{
    LootableVault(0, 3, "0172");
    close;

OnInit:
    .distance=3;
    end;

OnClock0201:
OnClock1418:
    $VAULT_0172+=rand2(3,7);
    end;
}

