// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Sells beer

// Temporary Sprite
024-9,39,31,0	script	Bar Jobs	NPC_DWARF_TRADER,{
    hello;
    npcshopattach(.name$);
    shop .name$;
    close;

OnInit:
    tradertype(NST_MARKET);

    sellitem DwarvenSake, 1100, 1;
    sellitem Beer, 300, 20;
    if ($ARKIM_ST >= 4000)
        sellitem ClothoLiquor, -1, (($ARKIM_ST-4000)/500)+1;
    if ($ARKIM_ST >= 4700)
        sellitem LachesisBrew, -1, (($ARKIM_ST-4700)/500)+1;

    .sex = G_MALE;
    .distance = 5;
    end;

OnClock2358:
    restoreshopitem DwarvenSake, 1100, 1;
    restoreshopitem Beer, 300, 20;
    if ($ARKIM_ST >= 4000)
        restoreshopitem ClothoLiquor, (($ARKIM_ST-4000)/500)+1;
    if ($ARKIM_ST >= 4700)
        restoreshopitem LachesisBrew, (($ARKIM_ST-4700)/500)+1;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;

}

