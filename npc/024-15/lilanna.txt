// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Anise INC. Representative

024-15,23,24,0	script	Lilanna	NPC_ELF_F,{
    function FixCrystal;
    function FixSelect;
    mesn;
    mesq l("Hello there, I am Lilanna and you are in ANISE INC. headquarters.");
    next;
    goto L_Loop;

L_Loop:
    mesn;
    mesq l("How may I help you?");
    select
        l("Thanks."),
        l("What ANISE INC. Does?"),
        l("How to use best Warp Technology?"),
        l("I would like to buy a Warp Crystal."),
        rif(countitem(BrokenWarpCrystal), l("My warp crystal broke!"));
    mes "";
    switch (@menu) {
        case 1:
            close;
        case 2:
            mesn;
            mesq l("ANISE Incorporated is the biggest company in warp technology!");
            next;
            mesn;
            mesq l("From magical gates to ancient time-space technology - we know it all!");
            next;
            mesn;
            mesq l("Do you know the warp crystals? We did them. Did you saw the great warp gate at the town entrance? It was us!");
            next;
            mesn;
            mesq l("If you ever need to get quickly from one place to the other, count on us!");
            next;
            break;
        case 3:
            mesn;
            mesq l("All warp technology, be it crystal-based, potion-based, or mechanic-based, relies on timespace distortions.");
            next;
            mesn;
            mesq l("If you distort time-space too much, you might end up outside of it, meeting a quick, permanent, and unrecoverable death.");
            next;
            mesn;
            mesq l("Different warp systems overlaps each other, so the cooldown is carried over regardless of the technology you use.");
            mesc l("Potentially more destructive warp systems will have much longer cooldowns because they cause bigger holes in timespace.");
            next;
            mesn;
            mes l("How to better preserve your dispostives:");
            mes l("Mechanically based technology (eg. the Gates or Time Flasks) will never suffer damage from operation but are more costly.");
            mes l("Crystal based technology can break if used too often. Try taking longer between each warp to raise their lifetime.");
            mes l("Potion based technology will vanish upon use and usually don't interfer with other technologies. Usually.");
            next;
            mesn;
            mesq l("Knowing when to warp is the secret to success!");
            next;
            break;
        case 4:
            npcshopattach(.name$);
            openshop();
            closeclientdialog;
            end;
            break;
        case 5:
            FixSelect();
            break;
    }
    goto L_Loop;

// FixCrystal ( Crystal ID, GP Tax )
function FixCrystal {
    .@cy=getarg(0);
    .@gp=getarg(1);
    if (Zeny < .@gp) {
        mesc l("NOT ENOUGH MONEY"), 1;
        mesc l("@@/@@ GP", fnum(Zeny), fnum(.@gp));
        next;
        return;
    }
    inventoryplace .@cy, 1;
    delitem BrokenWarpCrystal, 1;
    Zeny-=.@gp;
    getitem .@cy, 1;
    mesc l("Operation successful."), 3;
    next;
    mesn;
    mesq l("Here you go, a brand new crystal. I'll fix the old one on the meanwhile. Take care of it.");
    next;
    return;
}

// A menu for fixing crystals
function FixSelect {
    do
    {
        mesc l("Fixing Crystals have a price. You can only charge crystals to places you've already been.");
        mesc l("These are saved by walking in warp portals or touching Soul Menhirs.");
        mes l("You have: @@ Broken crystals", countitem(BrokenWarpCrystal));
        .@cbase=750; // Candor Base
        .@nbase=1250; // Normal Base
        .@lbase=2500; // LoF Base
        .@fbase=5000; // Faraway Base
        .@kbase=10000; // King Base (incl. Fortress Is.)
        select
            l("Don't fix."),
            rif(true, l("Candor - @@ GP", fnum(.@cbase))),
            rif(TELEPORTERS & TP_TULIM, l("Tulimshar - @@ GP", fnum(.@nbase))),
            rif(TELEPORTERS & TP_HURNS, l("Hurnscald - @@ GP", fnum(.@nbase))),
            rif(TELEPORTERS & TP_NIVAL, l("Nivalis - @@ GP", fnum(.@nbase))),
            rif(TELEPORTERS & TP_FROST|TP_HALIN, l("Land Of Fire - @@ GP", fnum(.@lbase))),
            rif(TELEPORTERS & TP_HALIN, l("Halinarzo - @@ GP", fnum(.@fbase))),
            rif(TELEPORTERS & TP_FROST, l("Frostia - @@ GP", fnum(.@fbase))),
            rif(TELEPORTERS & TP_LILIT, l("Lilit - @@ GP (not exchangeable)", fnum(.@kbase)));
        mes "";
        switch (@menu) {
            case 1: return;
            case 2: FixCrystal(CandorWarpCrystal, .@cbase); break;
            case 3: FixCrystal(TulimWarpCrystal, .@nbase); break;
            case 4: FixCrystal(HurnsWarpCrystal, .@nbase); break;
            case 5: FixCrystal(NivalWarpCrystal, .@nbase); break;
            case 6: FixCrystal(LoFWarpCrystal, .@lbase); break;
            case 7: FixCrystal(HalinWarpCrystal, .@fbase); break;
            case 8: FixCrystal(FrostiaWarpCrystal, .@fbase); break;
            case 9: FixCrystal(LilitWarpCrystal, .@kbase); break;
        }
    } while (countitem(BrokenWarpCrystal));
    return;
}

OnTimer1000:
    domovestep;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Monocle);
    setunitdata(.@npcId, UDT_HEADMIDDLE, AlchemistArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonSkirt);
    setunitdata(.@npcId, UDT_WEAPON, FurBoots); // She could use gloves, no
    setunitdata(.@npcId, UDT_HAIRSTYLE, 19);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 16);

    initpath "move", 23, 24,
             "dir", DOWN, 0,
             "wait", 15, 0,
             "dir", RIGHT, 0,
             "move", 27, 24,
             "wait", 3, 0,
             "dir", DOWN, 0,
             "wait", 15, 0,
             "move", 24, 32,
             "wait", 3, 0,
             "dir", RIGHT, 0,
             "wait", 10, 0,
             "dir", LEFT, 0,
             "wait", 10, 0,
             "dir", UP, 0,
             "wait", 3, 0,
             "move", 27, 34,
             "dir", UP, 0,
             "wait", 3, 0,
             "dir", LEFT, 0,
             "wait", 3, 0,
             "move", 23, 24,
             "wait", 3, 0,
             "dir", DOWN, 0,
             "wait", 10, 0;
    initialmove;
    initnpctimer;
    tradertype(NST_MARKET);
    sellitem BrokenWarpCrystal, 50000, 1;

    .sex = G_MALE;
    .distance = 5;
    end;

OnClock2358:
    restoreshopitem BrokenWarpCrystal, 50000, 1;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;
}

