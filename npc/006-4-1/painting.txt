// TMW2 Script
// Author:
//    Jesusalva

006-4-1,59,29,0	script	#jakPortrait1	NPC_NO_SPRITE,{
    RegEasterEgg(EE_JAK1, 1);
    mesc ".:: " + l("Researcher Jak's Residence") + " ::.";
    mes "";
    mesc l("And his sister,");
    mes "";
    mesc ".:: " + l("Researcher Alyta's Residence") + " ::.";
    close;

OnInit:
    .distance=2;
    end;
}

006-4-1,24,53,0	script	#jakWestWind	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    dispbottom l("This passage is still sealed.");
    end;
}

