// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 006-4-1: Abandoned Building warps
006-4-1,27,108,0	script	#006-4-1_27_108	NPC_HIDDEN,0,1,{
	end;
OnTouch:
	slide 87,53; end;
}
006-4-1,86,103,0	script	#006-4-1_86_103	NPC_HIDDEN,0,1,{
	end;
OnTouch:
	slide 40,160; end;
}
006-4-1,39,160,0	script	#006-4-1_39_160	NPC_HIDDEN,0,1,{
	end;
OnTouch:
	slide 85,103; end;
}
006-4-1,58,61,0	warp	#006-4-1_58_61	6,0,006-4,95,100
006-4-1,88,53,0	script	#006-4-1_88_53	NPC_HIDDEN,0,1,{
	end;
OnTouch:
	slide 28,108; end;
}
006-4-1,44,30,0	warp	#006-4-1_44_30	0,0,006-4,70,82
