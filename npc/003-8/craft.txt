// TMW2: Moubootaur Legends scripts.
// Author:
//    Jesusalva
// Description:
//    Rentable Craft Bench

003-8,35,30,0	script	Crafting Table	NPC_NO_SPRITE,{
    do {
        mesc l("What will you craft today?");
        mesc l("It costs 600 GP to use."), 1;
        if (Zeny < 600)
            close;

        if (SmithSystem(CRAFT_PLAYER)) {
            if (Zeny < 600) {
                mesc l("WARNING, you have been detected cheating and thus, violating Tulimshar Anti-Theft Policy."), 1;
                mesc l("You were jailed and now need a GM to get you out of there."), 1;
                logmes "WARNING, "+strcharinfo(0)+" found out cheating, only had "+Zeny+"/600 GP for craft table. Jailed.", LOGMES_ATCOMMAND;
                atcommand("@jail "+strcharinfo(0));
                // Apply a more realistic penalty
                Zeny=0;
                CRAFTSYS_CURRENT=0;
                close;
            }
            Zeny-=600;
            mesc l("Success!"), 3;
        } else {
            mesc l("That didn't work!"), 1;
        }
        next;
        mesc l("Try again?");
    } while (askyesno() == ASK_YES);

    close;

OnInit:
    .distance=3;
    end;
}


