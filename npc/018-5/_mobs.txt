// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-5: Lilit Island mobs
018-5,68,87,25,16	monster	Water Fairy	1185,8,20000,40000
018-5,66,84,25,18	monster	Grass Snake	1169,8,10000,10000
018-5,82,88,41,15	monster	Wind Fairy	1185,8,40000,20000
018-5,99,47,22,14	monster	Poison Fairy	1186,6,30000,30000
018-5,58,146,26,12	monster	Mountain Snake	1123,6,120000,0,Shaabty::OnKillSnake
018-5,58,83,18,22	monster	Green Dragon	1195,2,120000,0
018-5,100,52,21,18	monster	Squirrel	1055,6,30000,30000
018-5,102,88,22,14	monster	Vanity Pixie	1215,2,320000,0
018-5,86,96,18,10	monster	Nulity Pixie	1218,1,320000,0
