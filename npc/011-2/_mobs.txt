// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 011-2: Supreme Mana Cave mobs
011-2,55,58,29,22	monster	Cave Snake	1035,12,35000,150000
011-2,70,40,3,1	monster	Small Topaz Bif	1101,1,35000,150000
011-2,54,54,54,54	monster	Cave Maggot	1027,20,40000,200000
011-2,43,66,9,6	monster	Snake	1122,3,35000,150000
011-2,46,40,4,4	monster	Snake	1122,3,35000,150000
011-2,84,56,9,9	monster	Snake	1122,3,35000,150000
011-2,60,46,29,22	monster	Cave Maggot	1027,6,35000,150000
011-2,55,48,29,22	monster	Small Topaz Bif	1101,3,35000,150000
