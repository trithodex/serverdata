// TMW2 Script, ported from TMW-BR
// TMW2 Author: Jesusalva
//
//  Creator: Cardinalli
//  Review: Lunovox <rui.gravata@gmail.com>
//
//  Description:
//    A rather easy quest to give players what to kill.
//  Reward:
// Mouboo pendant + 20% exp of level 55

009-5,36,35,4	script	Joaquim	NPC_PLAYER,{
    .@q=getq(HalinarzoQuest_SickWife);
    if (BaseLevel < 36) goto L_TooWeak;
    if (BaseLevel < 55) goto L_Weak;
    if (.@q == 5) goto L_Complete;
    if (.@q == 4) goto L_Finish;
    if (.@q == 3) goto L_DoIt;
    if (.@q == 2) goto L_Return;
    if (.@q == 1) goto L_Found;
    goto L_Start;

L_TooWeak:
    mesn;
    mesq l("Here is a safe haven for we who don't fight. There are no monsters, and the Mouboo watches over us.");
    close;

L_Weak:
    mesn;
    .@r=rand2(1,5);
    switch (.@r) {
    case 1:
        mesq l("Watch out! My wife was gravely wounded the other day. Don't forget your shield when leaving this holy place!");
        break;
    case 2:
        mesq l("Ah, I hate mushrooms. Perhaps in future, I could use their spikes and mushies.");
        break;
    case 3:
        mesq l("Ah, I hate snakes. Perhaps in future, I could use their tongues.");
        break;
    case 4:
        mesq l("Ah, I love mouboos. But their steaks, hmm. Ah, no, I shouldn't eat that...");
        break;
    case 5:
        mesq l("You should never sell your Cactus Drinks. They have many uses.");
        break;
    }
    close;

L_Complete:
    mesn;
    mesq l("Thanks for helping my wife, I'll be forever grateful.");
    close;

L_Finish:
    inventoryplace ElixirOfLife, 1, MoubooPendant, 1;
    getitem ElixirOfLife, 1;
    getitem MoubooPendant, 1;
    getexp 39260, 75;
    setq HalinarzoQuest_SickWife, 5;
    mesn;
    mesq l("Take this spare @@ I did. It heals fully and instantly, so don't hesit to use it if you're about to die.", getitemlink(ElixirOfLife));
    next;
    mesn;
    mesq l("Thanks for helping my wife! Here is, an @@. May the Mouboo watch over you! o.o", getitemlink(MoubooPendant));
    close;

L_DoIt:
    mesn;
    mesq l("Please help my wife Yumi, on the Hospital!");
    close;

// Quest Core
L_Start:
    mesn;
    mesq l("Ohhhhh..... Please, help me!!! My wife is gravely wounded!!");
    next;
    mesn strcharinfo(0);
    mesq l("Calm down! How can I help you?");
    next;
    mesn;
    mesq l("My grandmother gave me a recipe of the @@, it can cure anything but death.", getitemlink(ElixirOfLife));
    next;
    mesn;
    mesq l("I don't remember what I need now, but if you give me a moment, I'll get the list.");
    setq HalinarzoQuest_SickWife, 1;
    close;

L_Found:
    mesn;
    mesq l("I can make an @@, I still have a bottle of fairy blood, a few mana pearls, and some other rare ingredients.", getitemlink(ElixirOfLife));
    mesq l("It is the non-rare ingredients I actually need help with!");
    next;
    mesn l("@@ Recipe", getitemlink(ElixirOfLife));
    mesc l("@@/100 @@", countitem(CactusDrink), getitemlink(CactusDrink));
    mesc l("@@/60 @@", countitem(HardSpike), getitemlink(HardSpike));
    mesc l("@@/45 @@", countitem(SmallMushroom), getitemlink(SmallMushroom));
    mesc l("@@/40 @@", countitem(SnakeTongue), getitemlink(SnakeTongue));
    mesc l("@@/30 @@", countitem(BottleOfTonoriWater), getitemlink(BottleOfTonoriWater));
    mesc l("@@/20 @@", countitem(CaveSnakeTongue), getitemlink(CaveSnakeTongue));
    mesc l("@@/15 @@", countitem(MoubooSteak), getitemlink(MoubooSteak));
    next;
    select
        l("I will do it, don't worry."),
        rif(countitem(ElixirOfLife), l("I have one here...")),
        l("Ahh, too many items. Sorry.");
    mes "";
    mesn;
    if (@menu == 1) {
        mes lg("Thanks! Thanks! Savior! Hurry up!");
        setq HalinarzoQuest_SickWife, 2;
    } else if (@menu == 2) {
        mes l("Uhm, sorry, I don't trust stuff you get at market. You know.");
        mes l("Full of agrotoxins, transgenics and whatever. Not safe.");
    } else {
        mes l("Oh noes, who nows can help my wife? Please reconsider!");
    }
    close;

L_Return:
    mesn l("@@ Recipe", getitemlink(ElixirOfLife));
    mesc l("@@/100 @@", countitem(CactusDrink), getitemlink(CactusDrink));
    mesc l("@@/60 @@", countitem(HardSpike), getitemlink(HardSpike));
    mesc l("@@/45 @@", countitem(SmallMushroom), getitemlink(SmallMushroom));
    mesc l("@@/40 @@", countitem(SnakeTongue), getitemlink(SnakeTongue));
    mesc l("@@/30 @@", countitem(BottleOfTonoriWater), getitemlink(BottleOfTonoriWater));
    mesc l("@@/20 @@", countitem(CaveSnakeTongue), getitemlink(CaveSnakeTongue));
    mesc l("@@/15 @@", countitem(MoubooSteak), getitemlink(MoubooSteak));
    next;
    select
        l("I'll be back later with all ingredients."),
        l("They're with me.");
    mes "";
    if (@menu == 1)
        close;

    if (countitem(CactusDrink) < 100 ||
        countitem(HardSpike) < 60 ||
        countitem(SmallMushroom) < 45 ||
        countitem(SnakeTongue) < 40 ||
        countitem(BottleOfTonoriWater) < 30 ||
        countitem(CaveSnakeTongue) < 20 ||
        countitem(MoubooSteak) < 15)
            goto L_Missing;

    inventoryplace ElixirOfLife, 1;

    delitem CactusDrink, 100;
    delitem HardSpike, 60;
    delitem SmallMushroom, 45;
    delitem SnakeTongue, 40;
    delitem BottleOfTonoriWater, 30;
    delitem CaveSnakeTongue, 20;
    delitem MoubooSteak, 15;
    getitem ElixirOfLife, 1;
    setq HalinarzoQuest_SickWife, 3;
    mesn;
    mesq l("Thanks, I'll just bake the Elixir right away...!");
    next;
    mesc l("@@ goes away for a while and returns briefly.", .name$);
    next;
    mesn;
    mesq l("Here, take the Elixir. Please, bring it to my wife! I am counting on you!!");
    close;

L_Missing:
    mesn strcharinfo(0);
    mesq l("Except they're not. I'll be back later.");
    next;
    mesn;
    mesq l("Please, @@! Hurry up!", strcharinfo(0));
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 3);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);
    npcsit;

    .sex = G_MALE;
    .distance = 5;
    end;
}
