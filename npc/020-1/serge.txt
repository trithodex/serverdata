// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Hunt Fluffies, and Winter Quest, based on Luffyx. Disabled during Christmas.
//
//    SQuest_Winter
//    SergeQuest;

020-1,77,78,0	script	Serge	NPC_ELF,{
    if (season() != WINTER)
        goto L_OutOfSeason;
    if ($EVENT$ == "Christmas")
        goto L_QuestDisabled;

    .@q=getq(SQuest_Winter);
    mesn;
    mesq l("Brrrr... I'm freezing! The winter at Nivalis is so harsh!!");
    if (.@q < 1)
        goto L_WinterQuest;

L_Main:
    if (GHQUEST)
        GHQ_Assign(Fluffy, "Nivalis");
    close;

L_OutOfSeason:
    mesn;
    mesq l("Hmm, Nivalis is a wonderful place to live in! Although it is a tad too cold on Winter...");
    goto L_Main;

L_QuestDisabled:
    mesn;
    mesq l("Merry Christmas!");
    mesc l("The @@ quest is disabled during Christmas event.", getitemlink(KnitHat));
    next;
    goto L_Main;

L_WinterQuest:
    next;
    mesn;
    mesq l("Hey, do you know what is good on this harsh winter? A @@!", getitemlink(KnitHat));
    next;
    mesn;
    mes l("What about you bring me:");
    mes l("@@/120 @@", countitem(Snowflake), getitemlink(Snowflake));
    mes l("@@/80 @@", countitem(CaramelCandy), getitemlink(CaramelCandy));
    mes l("@@/40 @@", countitem(GingerBreadMan), getitemlink(GingerBreadMan));
    mes l("@@/10 @@", countitem(ChocolateBiscuit), getitemlink(ChocolateBiscuit)); // Found at Chocolate Slime: 4.00%
    next;
    select
        l("Not now, thanks"),
        l("To be honest, I have that with me!");

    mes "";
    if (@menu == 1)
        goto L_Main;
    if (@menu == 2) {
        if (
            countitem(Snowflake)        < 120 ||
            countitem(CaramelCandy)     < 80 ||
            countitem(GingerBreadMan)   < 40 ||
            countitem(ChocolateBiscuit) < 10
            ) goto L_Lying;

        inventoryplace KnitHat, 1;
        delitem Snowflake, 120;
        delitem CaramelCandy,  80;
        delitem GingerBreadMan, 40;
        delitem ChocolateBiscuit, 10;
        getitem KnitHat, 1;
        getexp BaseLevel*267, JobLevel*80; // Level 30/20 ABSOLUTE CAP
        setq1 SQuest_Winter, 1;
        mesn;
        mesq l("Yay yay! Many thanks! Here, take the reward as promised!");
        next;
        mesn;
        mesq l("We can do this again on next winter!");
        goto L_Main;
    }

    close;

L_Lying:
    mesn;
    mesq l("Please don't lie to me...");
    goto L_Main;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, CommunityShirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, JeansChaps);
    setunitdata(.@npcId, UDT_HEADBOTTOM, DeepBlackBoots);
    setunitdata(.@npcId, UDT_WEAPON, KnitHat);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 13);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .distance=4;
    .sex=G_MALE;
    npcsit;
	end;

}
