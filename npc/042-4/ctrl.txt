// TMW 2 Script
// Author:
//  Jesusalva
//  Micksha
// Description:
//  Controls sewers.
//  This one is simple, it just sends a message about path splitting
//  And spawn several monsters when instance is initialized D:
//  Also handles their respawn.

// A simple random treasure chest - to be sure players were introduced to this
// awesome system. Same rules as any treasure box still applies.
042-4,94,47,0	script	#chest_0424	NPC_CHEST,{
    function monster0424;
	TreasureBox();
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;

OnInit:
	.distance = 2;
	end;

OnInstanceInit:
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        debugmes "[ERROR] [KAMELOT] Unable to spawn for Kamelot %s", .map$;
        debugmes "[ERROR] [KAMELOT] Using dummy data (returned: %d)", .@g;
        .@g=0;
    }
    debugmes "Spawning monsters for guild %d", .@g;
    .@mx=getguildavg(.@g);
    monster0424(1, 20, 20, 115, 100, .@mx);
    monster0424(4, 20, 51,  51,  71, .@mx);
    monster0424(5, 85, 56, 115, 100, .@mx);
    monster0424(2, 79, 40,  97,  52, .@mx);
    monster0424(5, 51, 20,  80,  50, .@mx);

    // Neutral monsters
    areamonster(.map$, 20, 20, 115, 100, strmobinfo(1, Blub), Blub, 5);
    areamonster(.map$, 20, 20, 115, 100, strmobinfo(1, ManaGhost), ManaGhost, max(1, .@mx/10));

    // Bonus monsters
    if (!rand2(3))
        areamonster(.map$, 20, 20, 115, 100, "Micksha's Tortuga", Tortuga, 1);
    end;

OnKillMob:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getexp $KAMELOT_MX[.@g]*7, $KAMELOT_MX[.@g]*3;
    .@delay=max(7000, 42000-$KAMELOT_PC[.@g]*2000);
    // FALLTHROUGH

OnRespawn:
    .@delay=(.@delay ? .@delay : 7000);
    sleep(.@delay);
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        debugmes "[ERROR] [KAMELOT] Unable to respawn for Kamelot %s", .map$;
        .@g=0;
    }
    monster0424(1, 20, 20, 115, 100, $KAMELOT_MX[.@g]);
    end;

function monster0424 {
    .@label$=instance_npcname(.name$)+"::OnKillMob";
    .@gcount=getarg(0);
    .@x1=getarg(1);
    .@y1=getarg(2);
    .@x2=getarg(3);
    .@y2=getarg(4);
    .@avg=getarg(5);
    .@m$=instance_mapname("042-4");
    //debugmes "Total %d, map %s (power %d)", .@gcount, .@m$, .@avg;
    freeloop(true);
    for (.@i=0; .@i < .@gcount; .@i++) {
        .@mobId=any(CursedSoldier, CursedArcher); // 50-50 ratio
        .@mob=areamonster(.@m$, .@x1, .@y1, .@x2, .@y2, strmobinfo(1, .@mobId), .@mobId, 1, .@label$);
        // Reconfigure the monster
        setunitdata(.@mob, UDT_LEVEL, .@avg);
        setunitdata(.@mob, UDT_STR, 1+.@avg*3/10);
        setunitdata(.@mob, UDT_AGI, 1+.@avg*3/10);
        setunitdata(.@mob, UDT_VIT, 1+.@avg*3/10);
        setunitdata(.@mob, UDT_INT, 1+.@avg*3/10);
        setunitdata(.@mob, UDT_DEX, 1+.@avg*3/10);
        setunitdata(.@mob, UDT_LUK, 1+.@avg*3/10);
        setunitdata(.@mob, UDT_ADELAY, 1572);
        setunitdata(.@mob, UDT_ATKRANGE, (.@mobId == CursedArcher ? any(6,7) : any(1,2)));
        // Battle Status
        setunitdata(.@mob, UDT_MAXHP,    .@avg*36);
        setunitdata(.@mob, UDT_HP,       .@avg*36);
        setunitdata(.@mob, UDT_ATKMIN,   .@avg*42/10);
        setunitdata(.@mob, UDT_ATKMAX,   .@avg*62/10);
        setunitdata(.@mob, UDT_DEF,      1+.@avg*10/10);
        setunitdata(.@mob, UDT_MDEF,     1+.@avg*6/10);
        setunitdata(.@mob, UDT_HIT,      .@avg*55/10); // Advised: x3
        setunitdata(.@mob, UDT_FLEE,     .@avg*37/10); // Advised: x4
        // Critical calculation
        .@min=5;
        .@max=max(.@min, min(30, .@avg/4));
        setunitdata(.@mob, UDT_CRIT,     rand2(.@min, .@max));
        // Loop through
    }
    freeloop(false);
    return;
    }

}


