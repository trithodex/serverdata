// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW LoF (Paxel)
// Description:
//    Informs about the Slime Cage

020-7-2,25,51,0	script	Parcival	NPC_BLUESAGEWORKER_MA,{
    mesn;
    mesq l("We keep the Slimes past the barrier for the night, so be careful when entering.");
    next;
    mesn;
    mesq l("A good thing we still use sturdy standard steel grating on the emergency exit which lead to Nivalis. I can't imagine how bad the incident would be wasn't for that.");
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}


