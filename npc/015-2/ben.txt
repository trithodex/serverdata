// TMW2 Scripts.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Ben Parkison invites players to the TMW2 Project
//    Part of the THIEF/MERCHANT branches

015-2,269,172,0	script	Ben Parkison	NPC_MOUBOO,{
    if (JobLevel > 20 && MERC_RANK == 0) goto L_Quest;

L_Intro:
    mesn;
    mesq l("Hey newbie... I am a dangerous bandit.");
    next;
    mesq l("Yes, I am a mouboo. Why? Can't a mouboo be a dangerous bandit?!");
    next;
    mesq l("Fear not! You can check our [@@https://wiki.moubootaurlegends.org|Wiki@@] to find that and other awesome stuff!");
    //mesq l("Anyway, you can check our [@@https://gitlab.com/TMW2/Docs/wikis/home|Wiki@@] to find awesome stuff!");
    mesc l("(Even bandits doesn't breaks the rules. To see the rules, use ##B@rules##b.)");
    next;
    mesq l("You can even join the project there. Contributors are greatly appreciated! %%N");
    close;

L_Quest:
    if (THIEF_RANK == 0)
        goto L_Recruit;
    mesn;
    mesq l("Hello there, @@ the @@.", strcharinfo(0), thiefrank());
    if (THIEF_RANK == 5) goto L_Menu;
    mesq l("I see you have collected some experience. Let me try to rank you up!");
    next;
    if (THIEF_EXP > (THIEF_RANK*2)**5) {
        THIEF_EXP-=(THIEF_RANK*2)**5;
        THIEF_EXP=(THIEF_EXP*8/10); // Penalty for postponing rank up (you keep 80%)
        THIEF_RANK+=1;
        mesn;
        mesq l("Congrats! You rank up! You are now a(n) @@!", thiefrank());
        if (THIEF_RANK == 2) goto L_Rank2;
        if (THIEF_RANK == 3) goto L_Rank3;
        if (THIEF_RANK == 4) goto L_Rank4;
    } else {
        mesn;
        mesq l("Well, you need more experience. Keep trying!");
        mesc l("You may obtain Thief Exp by using @@.", getitemlink(Lockpicks));
    }
    goto L_Menu;
    close;

L_Recruit:
    mesn;
    mesq l("You seem to be doing some money. Would you consider fighting for your own greed, or even be a Robin-Hood-of-sorts, stealing from the rich?");
    //mesc l("Notice: If you join the ##BBandits Guild##b now, you WON'T BE ABLE to be a merchant police later!"), 1;
    mesc l("Note: You won't be able to leave the class later."), 1;
    next;
    if (askyesno() == ASK_YES) {
        THIEF_EXP=0;
        THIEF_RANK=1;
        mes "";
        mesn;
        mesq l("Welcome to the ##BThieves Guild##b! Follow those with higher rank than you, and happy stealing!");
        mesc l("You've learned how to use @@. Simple locks can now be broken.", getitemlink(Lockpicks));
    } else {
        goto L_Intro;
    }
    close;

// Learn STEAL
L_Rank2:
    skill(TF_STEAL,1,0);
    next;
    mesn;
    mesq l("Now, you'll learn a thief trick! This one allows you to steal drops from monsters! Ka-pow! Amazing!");
    next;
    mesn;
    mesq l("You must be close to it, and stealing won't change drops! If you fail, just try again! Good luck!");
    close;

// Learn INCCARRY
L_Rank3:
    skill(ALL_INCCARRY,1,0);
    next;
    mesn;
    mesq l("Now, you'll learn a thief trick! What sort of thief loots so much that they get overweight penalty? That's not cool!");
    next;
    mesn;
    mesq l("You now gained two extra kilograms to your weight quota! Ka-pow, that's fantastic! Good luck!");
    close;

// Learn OVERCHARGE
L_Rank4:
    skill(MC_OVERCHARGE,1,0);
    next;
    mesn;
    mesq l("Now, you'll learn an important trick! Stealing is nice, but scamming is even better!");
    mesc l("We must blame Saulc!");
    next;
    mesn;
    mesq l("This new skill will allow you to pinch every gold piece from a NPC when selling stuff! Ka-pow, now you can steal items and resell them for moar GP! AMAZING!");
    close;

// Allow to level up thief skills
L_Menu:
    next;
    mesn;
    mesq l("Do you want me to teach you how to improve an existing skill with MAGIC? There are no better mages than Mouboos!");
    mesc l("You also need @@/@@ Mob Points to improve thief skills.", 1000, format_number(Mobpt));
    if (Mobpt < 1000)
        close;
    next;
    /*
    menuint
        rif(getskilllv(TF_STEAL), l("Stealing")), TF_STEAL,
        rif(getskilllv(TF_STEAL), l("Stealing")), ALL_INCCARRY,
        rif(getskilllv(TF_STEAL), l("Stealing")), MC_OVERCHARGE,
        l("None at the moment."), 0;

    // Handle result
    if (@menuret) {
        if (!learn_magic(@menuret))
            mesc l("You do not meet all requisites for this skill."), 1;
    } else {
        close;
    }
    */

    select
        rif(getskilllv(TF_STEAL), l("Improve Stealing to level ")+(getskilllv(TF_STEAL)+1)),
        rif(getskilllv(ALL_INCCARRY), l("Improve Max Weight to level ")+(getskilllv(ALL_INCCARRY)+1)),
        rif(getskilllv(MC_OVERCHARGE), l("Improve Barter to level ")+(getskilllv(MC_OVERCHARGE)+1)),
        l("None at the moment.");
    mes "";
    // BlueCoral, {CrocClaw, OceanCrocClaw: Empty Box}, PlushroomBox
    switch (@menu) {
        case 1:
            if (!mlearn(TF_STEAL, 10, 1, BlueCoral, 6*getskilllv(TF_STEAL)))
                mesc l("You do not meet all requisites for this skill."), 1;
            else
                Mobpt-=1000;
            break;
        case 2:
            if (!mlearn(ALL_INCCARRY, 10, 1, BlueCoral, 9*getskilllv(ALL_INCCARRY)))
                mesc l("You do not meet all requisites for this skill."), 1;
            else
                Mobpt-=1000;
            break;
        case 3:
            if (!mlearn(MC_OVERCHARGE, 8, 1, PlushroomBox, 2*getskilllv(MC_OVERCHARGE))) // Max 21% discount (out of 24%)
                mesc l("You do not meet all requisites for this skill."), 1;
            else
                Mobpt-=1000;
            break;
        default:
            close;
    }
    goto L_Menu;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

