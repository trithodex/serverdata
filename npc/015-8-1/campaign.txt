// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Sagratha Quest (field 3)

015-8-1,49,38,0	script	#SaggyDungeonCore	NPC_NO_SPRITE,{
    // Not in instance, we don't care.
    if (instance_id() < 0)
        end;
    if (!.SAGRATHA)
        dispbottom l("I cannot read the signs from this far away.");
    end;

// There is no instance init
OnBegin:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);
    .PLAYERID=getcharid(3);

    // 255 = PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_COMMANDS|PCBLOCK_SITSTAND|PCBLOCK_IMMUNE|PCBLOCK_CHAT|PCBLOCK_MOVE
    setpcblock(255, true);
    setq3 HurnscaldQuest_Sagratha, 1;

    // Setup boss units
    .SAGRATHA=monster(.@m$, 52, 40, "Sagratha", Sagratha, 1, .@n$+"::OnSagrathaDie", Size_Medium, 2);
    .BOSS=monster(.@m$, 47, 40, l("Masked Assassin"), HoodedAssassin, 1, .@n$+"::OnSagrathaWin");

    unitstop(.SAGRATHA);
    unitstop(.BOSS);
    sc_start(SC_STUN, 9000, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, .SAGRATHA);
    sc_start(SC_STUN, 9000, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, .BOSS);

    // FIXME: Seems like this is an "unsafe command" in 4144's opinion
    // Prevent players from engaging Sagratha and allies
    clientcommand "addignoreattack Sagratha";
    clientcommand "addignoreattack "+l(strmobinfo(1, Mouboo));
    clientcommand "addignoreattack "+l(strmobinfo(1, ForestMushroom));
    clientcommand "addignoreattack "+strmobinfo(1, Mouboo);
    clientcommand "addignoreattack "+strmobinfo(1, ForestMushroom);

    unittalk(.SAGRATHA, l("What are you doing here, @@!", get_race()));

    addtimer(2000, .@n$+"::OnS02");
    end;

OnS02:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(.SAGRATHA, l("I don't need help!"));
    unittalk(.BOSS, l("Die already!"));

    addtimer(3000, .@n$+"::OnS03");
    end;

OnS03:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(.SAGRATHA, l("If you don't want to die, fight!"));
    unittalk(.BOSS, lg("Murder her too!", "Murder him too!"));

    addtimer(3000, .@n$+"::OnS04");
    end;

OnS04:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    // Assassin's Army
    areamonster .@m$, 44, 40, 55, 50, strmobinfo(1, HoodedNinja), HoodedNinja, 2, .@n$+"::OnError";
    areamonster .@m$, 44, 40, 55, 50, strmobinfo(1, Assassin), Assassin, 4, .@n$+"::OnError";

    // Sagratha's Army
    areamonster .@m$, 44, 40, 55, 50, strmobinfo(1, Mouboo), Mouboo, 5, .@n$+"::OnError", Size_Medium, 2;
    areamonster .@m$, 44, 40, 55, 50, strmobinfo(1, ForestMushroom), ForestMushroom, 2, .@n$+"::OnError", Size_Medium, 2;

    // Heal them because you should not be fighting (yet).
    setunitdata(.BOSS,     UDT_HP,    getunitdata(.BOSS,     UDT_MAXHP));
    setunitdata(.SAGRATHA, UDT_HP,    getunitdata(.SAGRATHA, UDT_MAXHP));
    unitstop(.SAGRATHA);
    unitstop(.BOSS);

    // Objective announce
    mapannounce(.@m$, "##2"+l("Victory Conditions: Protect Sagratha!"), 0);
    mapannounce(.@m$, "##2"+l("Victory Conditions: Defeat the assassin!"), 0);
    mapannounce(.@m$, "##1"+l("Defeat Conditions: Your death!"), 0);
    mapannounce(.@m$, "##1"+l("Defeat Conditions: Time run out!"), 0);
    mapannounce(.@m$, "##1"+l("Defeat Conditions: Sagratha gets killed!"), 0);

    // Status cleanup
    // 255 = PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_COMMANDS|PCBLOCK_SITSTAND|PCBLOCK_IMMUNE|PCBLOCK_CHAT|PCBLOCK_MOVE
    setpcblock(255, false);
    // TODO: After x time, reinforcements on both sides
    // BOSS: “Kill 'em all!”
    // SAGGY: “Beings of florest, come to my aid and protect me!”
    addtimer(20000, .@n$+"::OnW01");
    end;

OnW01:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(.BOSS, l("Kill 'em all!"));
    unittalk(.SAGRATHA, l("It's an ambush!"));

    monster .@m$, 39, 39, strmobinfo(1, HoodedNinja), HoodedNinja, 1, .@n$+"::OnError";
    monster .@m$, 60, 39, strmobinfo(1, HoodedNinja), HoodedNinja, 1, .@n$+"::OnError";

    monster .@m$, 39, 54, strmobinfo(1, Assassin), Assassin, 1, .@n$+"::OnError";
    monster .@m$, 60, 54, strmobinfo(1, Assassin), Assassin, 1, .@n$+"::OnError";
    addtimer(rand(10000,15000), .@n$+"::OnW02");
    end;

OnW02:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(.SAGRATHA, l("Mouboos, come to my aid and protect me!"));

    monster .@m$, 46, 59, strmobinfo(1, Mouboo), Mouboo, 1, .@n$+"::OnError", Size_Medium, 2;
    monster .@m$, 44, 61, strmobinfo(1, Mouboo), Mouboo, 1, .@n$+"::OnError", Size_Medium, 2;
    monster .@m$, 49, 61, strmobinfo(1, Mouboo), Mouboo, 1, .@n$+"::OnError", Size_Medium, 2;
    addtimer(30000, .@n$+"::OnW03");
    end;

OnW03:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(.SAGRATHA, l("Do not think you can defeat me yet!"));
    unittalk(.BOSS, l("You'll all die here!"));

    monster .@m$, 49, 34, strmobinfo(1, ForestMushroom), ForestMushroom, 1, .@n$+"::OnError", Size_Medium, 2;
    monster .@m$, 47, 36, strmobinfo(1, Assassin), Assassin, 1, .@n$+"::OnError";
    monster .@m$, 52, 36, strmobinfo(1, Assassin), Assassin, 1, .@n$+"::OnError";
    addtimer(20000, .@n$+"::OnW04");
    end;

OnW04:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(.SAGRATHA, l("It's far from over! You cannot defeat me!"));

    monster .@m$, 46, 59, strmobinfo(1, Wolvern), Wolvern, 1, .@n$+"::OnError", Size_Medium, 2;
    monster .@m$, 44, 61, strmobinfo(1, ForestMushroom), ForestMushroom, 1, .@n$+"::OnError", Size_Medium, 2;
    monster .@m$, 49, 61, strmobinfo(1, PoisonSpikyMushroom), PoisonSpikyMushroom, 1, .@n$+"::OnError", Size_Medium, 2;
    end;

// Assassin, HoodedNinja, HoodedAssassin (boss)
// Mouboo, ForestMushroom, Fluffy, Sagratha (boss)
OnSagrathaDie:
    .@n$=instance_npcname(.name$);
    addtimer(70, .@n$+"::OnSagrathaReallyDie", .PLAYERID);
    end;

OnSagrathaReallyDie:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    deltimer(.@n$+"::OnW01");
    deltimer(.@n$+"::OnW02");
    deltimer(.@n$+"::OnW03");
    killmonster(.@m$, .@n$+"::OnError");
    unitkill(.BOSS);
    mapannounce(.@m$, "##2"+l("You lose!"), 0);

    dispbottom l("You failed to protect Sagratha..."); // and will need to start the quest again...");
    setq1 HurnscaldQuest_Sagratha, 3;
    setq3 HurnscaldQuest_Sagratha, 0;
    die();
    end;

OnSagrathaWin:
    .@n$=instance_npcname(.name$);
    addtimer(70, .@n$+"::OnSagrathaReallyWin", .PLAYERID);
    end;

OnSagrathaReallyWin:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    if (ispcdead())
        end;

    deltimer(.@n$+"::OnW01");
    deltimer(.@n$+"::OnW02");
    deltimer(.@n$+"::OnW03");
    killmonster(.@m$, .@n$+"::OnError");
    mapannounce(.@m$, "##2"+l("You win!"), 0);
    mapannounce(.@m$, "##2"+l("You must go talk with the Obelisk!"), 0);

    npctalk l("YOU WHO DEFILE THIS PLACE...");
    // How does she even knows your name?
    unittalk(.SAGRATHA, l("@@, watch out! The obelisk - it is talking!", strcharinfo(0)));
    unitstop(.SAGRATHA);

    addtimer(300, .@n$+"::OnSW01");
    end;

OnSW01:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unitwalk(.SAGRATHA, 50, 39);
    addtimer(2000, .@n$+"::OnSW02");
    end;

OnSW02:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    npctalk l("YOU WHO DEFILE THIS PLACE...");
    addtimer(2000, .@n$+"::OnSW03");
    end;

OnSW03:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    // Perhaps “Cursed” instead of “Sacred”?
    npctalk l("THIS IS THE MOUBOOTAUR TEMPLE RUINS... THIS PLACE IS SACRED...");
    addtimer(3000, .@n$+"::OnSW04");
    end;

OnSW04:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    npctalk l("THE BLOOD HERE SPILLED TODAY... ONLY ENRAGES HIM MORE...");
    unittalk(.SAGRATHA, l("Yikes, the Moubootaur!"));
    addtimer(3000, .@n$+"::OnSW05");
    end;

OnSW05:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    npctalk l("LEAVE NOW... BEFORE IT BECOMES TOO LATE....");
    addtimer(3000, .@n$+"::OnSW06");
    end;

OnSW06:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);
    if (getq(HurnscaldQuest_Sagratha) < 5)
        end;

    mesn l("Sagratha");
    mesq l("@@, we should leave here now.", strcharinfo(0));
    next;
    select
        l("What is the Moubootaur?"),
        l("I came here to ask for help with curses."),
        l("WHAT IN THE NAME OF THE FREAKING PENGUIN WAS THAT?!");
    mes "";
    mesn l("Sagratha");
    mesq l("That's none of your bussiness! We should leave here NOW!");
    next;
    mesn l("Sagratha");
    mesq l("You can tell me the details about why you're here on the way out...");
    next;
    mesn l("Sagratha");
    mesq l("But for now, we must NOT enrage the Moubootaur!!");
    next;
    setq HurnscaldQuest_Sagratha, 6, 0, 0;
    warp "014-5", 122, 139;
    clear;
    mesc l(".:: Victory ::."), 3;
    mesc l("You successfully completed Sagratha's Campaign."), 3;
    close;

OnError:
    end;

OnInit:
    .SAGRATHA=0;
    end;

}

// Dummy NPC to fire #SaggyDungeonCore when you get close to battle scene
015-8-1,49,39,0	script	#SaggyDungeonFire	NPC_HIDDEN,1,0,{
    end;

OnTouch:
    if (instance_id() < 0)
        end;

    if (!getq3(HurnscaldQuest_Sagratha)) {
        .@in=getq2(HurnscaldQuest_Sagratha);
        doevent(instance_npcname("#SaggyDungeonCore", .@in)+"::OnBegin");
    }
    end;

OnInit:
    .distance=0;
    end;

}


