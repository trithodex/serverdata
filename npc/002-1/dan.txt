// TMW-2 Script
// Authors:
//    Qwerty Dragon and Reid (originals)
//    Jesusalva
// Description:
//    Dan is a sailor from Nard ship (TODO another Redy? Seriously?) who is afraid
//    of Pious.
// Variables:
//    ShipQuests_Dan
// Translation:
//      FR Translated

002-1,32,31,0	script	Dan#002-1	NPC_REDY_MAN,{
    .@q=getq(ShipQuests_Dan);
    if (.@q == 1) goto L_Report;
    if (.@q == 2) goto L_Reward;
    if (.@q >= 3) goto L_Finished;

    mesn;
    mesq l("You see these pious around us?");
    next;
    mesq l("They're scary, don't you think so?");
    next;
    menu
        rif(getq(ShipQuests_Julia) < 3, l("Do you know where I can find Nard?")), L_Nard,
        l("Yeah, they are."), -,
        l("I am not afraid of Pious."), L_Quest;

    mes "";
    mesn;
    mesq l("I knew someone would agree with me!");
    close;

L_Quest:
    if (BaseLevel < 2) {
        mesn;
        mesq l("Well, you clearly never fought before, so remember to add stats.");
        mesc l("That can be done pressing F2 and opening char screen.");
        next;
    }
    mes "";
    mesn;
    mesq l("Then could you perhaps kill 12 @@ for me?!", getmonsterlink(Piou));
    next;
    menu
        l("They're too strong for me!"), -,
        l("Yes, of course. Without a sweat."), L_Continue;
    mes "";
    mesn;
    mesq l("Oh, please. They're weaker than Maggots. They will only pose a threat if you're unarmed.");
    close;

L_Continue:
    mes "";
    mesn;
    mesq l("I am counting on you!");
    setq ShipQuests_Dan, 1, 0;
    close;

L_Report:
    mesn;
    mesq l("You've killed @@/12 @@. Get rid of them!", getq2(ShipQuests_Dan),getmonsterlink(Piou));
    close;

L_Reward:
    mesn;
    mesq l("Woohoo, thank you! Maybe now they'll leave me alone!");
    next;
    mesn;
    mesq l("Here, take this pair of boots as a reward!");
    inventoryplace CreasedBoots, 1;
    getitem CreasedBoots, 1;
    getexp 25, 5;
    setq ShipQuests_Dan, 3, 0;
    next;
    mesn;
    mesq l("Peter was also looking for strong people to help him to kill other monsters.");
    if (BaseLevel < 8) {
        next;
        mesn;
        mesq l("You still have a low level but I'm sure he will ask for your help once you grow up a little more.");
    }
    close;

L_Finished:
    mesn;
    mesq l("It is no use... We are too close to a island, they will keep invading the ship...");
    next;
    mesn;
    mes l("(shivering) \"Ah, how I am afraid of pious!\"");
    close;

L_Nard:
    mesn;
    mesq l("Oh? Just go east of here. Keep going ##Bright##b, go down the stairs, and you'll be at his office already.");
    next;
    mesn;
    mesq l("And to leave this cramped ship, just go past his office! But beware...");
    next;
    mesn;
    mesq l("There are Pious on the island as well! %%i");
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

// TODO this code below is bad and should be entirely rewritten
002-1,0,0,0	script	PiouSpwn	NPC_HIDDEN,{
    function DanCheck {
        if (playerattached()) {
            // Quest not in progress - nothing to do
            if (getq(ShipQuests_Dan) != 1)
                return;

            // If you complete, finish it. Otherwise, sum it up.
            .@t=getq2(ShipQuests_Dan);
            if (.@t+1 >= 12)
                setq ShipQuests_Dan, 2, 0;
            else
                setq2 ShipQuests_Dan, .@t+1;

            // Report progress
            dispbottom l("@@/@@", .@t+1, 12);
        }
        return;
    }

OnFakeKill:
    DanCheck();
    end;

OnRespawnPiou1:
    DanCheck();
    sleep 39000;
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou1");
    end;
OnRespawnPiou2:
    DanCheck();
    sleep 41000;
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou2");
    end;
OnRespawnPiou3:
    DanCheck();
    sleep 39500;
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou3");
    end;
OnRespawnPiou4:
    DanCheck();
    sleep 41500;
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou4");
    end;
OnRespawnPiou5:
    DanCheck();
    sleep 40000;
    areamonster(instance_mapname("002-3"), 31, 26, 40, 31, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou5");
    end;

OnInstanceInit:
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou1");
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou2");
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou3");
    areamonster(instance_mapname("002-1"), 52, 32, 73, 41, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou4");
    areamonster(instance_mapname("002-3"), 31, 26, 40, 31, "Piou", Piou, 1, instance_npcname("PiouSpwn")+"::OnRespawnPiou5");
    end;

}
