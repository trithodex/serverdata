// TMW-2 scripts.
// Original Authors: Hal9000 & Qwerty Dragon
// TMW-2 Authors:
//    Ayruss
//    Jesusalva
// Description:
//    La Johanne Chef.
//    The cook may need help with something.
// Variable:
//    ShipQuests_ChefGado
//    ShipQuests_Knife
// Translation
//    FR Translated

002-1,27,28,0	script	Chef Gado	NPC_CHEF_GADO,{
    .@q = getq(ShipQuests_Knife);
    .@p = getq(ShipQuests_ChefGado);

    // Player should have the rusty knife already
    if (!.@q) goto L_Knife;

    // Piou quest
    if (.@q == 1 && .@p == 0) goto L_PiouLegs;
    if (.@p == 1) goto L_Continue;
    if (.@p == 2) goto L_Complete;

    // An error happened!
    close;

L_Knife:
    mesn;
    mesq l("I hate sea water, it always make knifes rusty. I already have a stockpile of rusty knifes on the other room.");
    close;

L_PiouLegs:
    mesn;
    mesq l("I'm trying to make good food for all the sailors here, could you help me?");
    mes "";
    menu
        l("Yes."), L_Start,
        l("Is there a reward?"), L_Ask,
        l("No."), -;
    close;


L_Ask:
    mes "";
    mesn;
    mesq l("I have a spare pair of gloves laying somewhere, you can have those if you finish the task.");
    mes "";
    menu
        l("Sounds good."), L_Start,
        l("No thanks."), -;
    close;

L_Start:
    mes "";
    mesn;
    mesq l("Great, I need 11 @@. Remember a good food makes a good crew.",getitemlink("PiouLegs"));
    setq ShipQuests_ChefGado, 1;
    mes "";
    menu
        l("I'll get to work."), -;
    close;


L_Continue:
    setq ShipQuests_ChefGado, 1;
    mesn;
    mesq l("Do you have the @@/11 @@ I requested? Sailors are getting hungry because you!",countitem("PiouLegs"),getitemlink("PiouLegs"));
    mes "";
    menu
        rif(countitem("PiouLegs") >= 11, l("Yes, take them.")), L_Reward,
        l("Not yet."), -;
    close;


L_Reward:
    inventoryplace CreasedGloves, 1;
    delitem PiouLegs, 11;
    getitem CreasedGloves, 1;
    setq ShipQuests_ChefGado,2;
    Zeny = Zeny + 100;
    getexp 25, 5;
    mes "";
    mesn;
    mesq l("Thanks. Take this spare pair of gloves and some change.");
    next;
    mesq l("I'm sure the crew will like to be spared from having to eat @@ again!", getitemlink(Piberries));
    close;
    end;

L_Complete:
    .@r = rand2(3);
    if (.@r == 0) npctalk3 l("What are you doing in my kitchen?! Get out, it's not a place for kids!");
    if (.@r == 1) npctalk3 l("Where is the salt?! This is sugar! Proper sailors need salt, not sugar!");
    if (.@r == 2) npctalk3 l("Are you going to stand here all day long? Go wash the dishes or go away.");
    closedialog;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 4;
    end;
}
