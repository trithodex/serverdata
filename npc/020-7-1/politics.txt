// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Town Administrator file, see npc/functions/politics.txt
// User variables:
//    #POL_APPLYWEEK = Week of last application
//    #POL_VOTEDAY = Day of last vote

020-7-1,37,61,0	script	Nivalis Office	NPC_POLITICS,{
do
{
    mesc ".:: "+l("Nivalis Townhall")+" ::.", 2;
    mesc l("Current Town Administrator: ")+$NIVAL_MAYOR$, 3;
    POL_TownInfo("NIVAL");
    mesc l("Application fee: @@ GP", .applytax);
    next;
    select
        l("Information"),
        rif(strcharinfo(0) == $NIVAL_MAYOR$, l("Manage Town")),
        rif(#POL_APPLYWEEK != gettimeparam(GETTIME_WEEKDAY), l("Apply for the office!")),
        l("View Candidate List and cast a vote"),
        l("[Quit]");

    switch (@menu) {
        case 1:
            POL_Information();
            break;
        case 2:
            POL_Manage("NIVAL");
            break;
        case 3:
            // array_push might be too sensible for getd/setd
            if (Zeny < .applytax)
                break;
            Zeny-=.applytax;
            $NIVAL_MONEY+=.applytax;
            #POL_APPLYWEEK=gettimeparam(GETTIME_WEEKDAY);
            array_push($NIVAL_CANDIDATE$, strcharinfo(0));
            array_push($NIVAL_VOTES, 0);
            mesc l("Application successful!"), 3;
            next;
            break;
        case 4:
            POL_Candidate("NIVAL");
            break;
        default:
            close;
    }
} while (true);
end;

OnInit:
    .applytax=100;
    .distance=4;
    end;
}

