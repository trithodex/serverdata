// TMW2/TMWBR Script
// Author:
//  Jesusalva
// Description:
//  Access to Lilit - He aids you in jumping off the bridge and reaching Lilit
//  Of course, to swim such large distance, you need to have plenty vit and str...
// Right spot: Between the crack (shallow) and the plant (shallow).

// .@q = LilitQuest_Access
// 0 - Access not granted
// 1 - Access granted
// 2 - Tree Minigame complete.

// TODO: Only allow "I want to swim" if the game knows you know about Lilit
// That is what Saulc wants, after all

017-1,160,227,0	script	#DrownedMan	NPC_NO_SPRITE,{
    if (!isin("017-1", 158, 222, 1))
        end;
    mesc l("Someone seems to be drowned in the water.");
    select
        l("Hello! Do you need help?"),
        l("Why shouldn't I jump here?"),
        l("But I want to swim!");
    mes "";
    switch (@menu)
    {
        case 1:
            mesn l("Drowned Man");
            mesq l("I'm afraid you can't help me... I still have one HP left.");
            next;
            mesn l("Drowned Man");
            mesq l("I also broke my bones, and I was equipping a weapon which prevents HP regen...");
            next;
            mesn l("Drowned Man");
            mesq l("I did some tritan friends, though. They teached me how to breath underwater. It's not so bad.");
            next;
            mesn l("Drowned Man");
            mesq l("In the first eight months I wanted to go back, but now I'm pretty used to living here. Please don't rescue me.");
            break;
        case 2:
            mesn l("Drowned Man");
            mesq l("Because it may be a fatal fall!");
            next;
            mesn l("Drowned Man");
            mesq l("Look the height of this bridge! Worse, the water around here is pretty shallow.");
            next;
            mesn l("Drowned Man");
            mesq l("Well, there might be a deep water spot, but it would still be dangerous.");
            next;
            mesn l("Drowned Man");
            mesq l("Unless you're looking forward a death penalty, DO NOT JUMP!");
            // We're in town, are you stupid >.>
            break;
        case 3:
            mesn l("Drowned Man");
            mesq l("Well, you see, you can't jump very far. And the water near land is shallow.");
            next;
            mesn l("Drowned Man");
            mesq l("I guess, there might be a single spot where water is a bit deeper, but I wasn't lucky.");
            next;
            mesn l("Drowned Man");
            mes l("Remember to store somewhere any heavy stuff you might be carrying.");
            mes l("I guess you cannot bring more than 1kg with full health. Less, if you're hurt.");
            next;
            mesn l("Drowned Man");
            mesq l("Also, you need vitality and strength to survive the swimming trip. The closest island is very far away.");
            mesc l("You need at least 35 str and 35 vit to do the trip safely - bonuses not counted - or you will lose HP."); // Can be safely mixed
            break;
    }
    close;

OnWarn:
    @lofcanjump=true;
    npctalkonce l("@@, don't jump there! It may be fatal!", strcharinfo(0));
    end;

// DO NOT LOWER
OnInit:
    .distance=7;
    end;
}

// Warning Event
017-1,158,222,0	script	#DrownedManArea	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    doevent("#DrownedMan::OnWarn");
    end;
OnInit:
    .distance=1;
    end;
}


// Any Jump. Code is obfuscated to inhibit cheaters :<
// Of course, I could use a blackbox too, but this is not the purpose.
// You don't want to solve the map hint, then you'll solve my formula!! /tableflip
017-1,162,226,0	script	#JumpArea01	NPC_NO_SPRITE,0,0,{
    // Do you want to jump?
    if (!@lofcanjump) {
        dispbottom l("What interesting water. Just don't ask me why.");
        end;
    }

    // Are you in the bridge?
    if (!isin("017-1", 158, 222, 1)) {
        dispbottom l("I'm too far to jump there.");
        end;
    }

    // Rate-limited? (3 seconds rate limit)
    if (@nojump >= gettimetick(2)+3) {
        dispbottom l("Gimme a sec to take a breath! I don't want to jump!");
        end;
    }

    mesc l("Are you really going to jump here?");
    next;
    if (askyesno() == ASK_YES) {
        // Can't jump with pets
        // FIXME: Move it elsewhere - resist pet_Detective updates
        for (.@i=0+PiouEgg; .@i <= 0+MoggunEgg ; .@i++) {
            if (countitem(.@i) > 0) {
                mesc l("Can't bring pets with you..."), 1;
                next;
                mesc l("Do you really think the poor creature should jump a bridge in water and DIE?"), 1;
                @nojump=gettimetick(2);
                mesc l("* Put the %s safely in storage before jumping.", getitemlink(.@i));
                close;
            }
        }
        // Do the jump!
        closeclientdialog;
        slide .x, .y;
        if (is_between(5220, 5270, .x*32) && is_between(7330, 7390, .y*32))
            goto L_GoodJump;
        dispbottom l("The water was too shallow...");
        die();
        end;
    }
    closeclientdialog;
    end;

L_GoodJump:
    dispbottom l("You jump in deep waters.");
    // You can only carry exact 2kg with full health.
    // Weight will be 2000g - so penalty will be 100% HP.
    .@penalty=max(0, (Weight/20)-1);
    percentheal -.@penalty, 0;

    // vit/str counts on swimming minigame
    mesn l("Drowned Man");
    mesq l("Good job! I wish you luck, because now you need to SWIM!");
    next;
    closeclientdialog;
    addtimer(3000, "#01850SwimmingCtrl::OnLoop");
    warp "018-5-0", 20, 25;
    end;

OnInit:
    .alwaysVisible=true;
    end;
}

017-1,162,227,0	duplicate(#JumpArea01)	#JumpArea02	NPC_NO_SPRITE
017-1,162,228,0	duplicate(#JumpArea01)	#JumpArea03	NPC_NO_SPRITE
017-1,162,229,0	duplicate(#JumpArea01)	#JumpArea04	NPC_NO_SPRITE
017-1,162,230,0	duplicate(#JumpArea01)	#JumpArea05	NPC_NO_SPRITE
017-1,162,231,0	duplicate(#JumpArea01)	#JumpArea06	NPC_NO_SPRITE

017-1,163,226,0	duplicate(#JumpArea01)	#JumpArea07	NPC_NO_SPRITE
017-1,163,227,0	duplicate(#JumpArea01)	#JumpArea08	NPC_NO_SPRITE
017-1,163,228,0	duplicate(#JumpArea01)	#JumpArea09	NPC_NO_SPRITE
017-1,163,229,0	duplicate(#JumpArea01)	#JumpArea10	NPC_NO_SPRITE
017-1,163,230,0	duplicate(#JumpArea01)	#JumpArea11	NPC_NO_SPRITE
017-1,163,231,0	duplicate(#JumpArea01)	#JumpArea12	NPC_NO_SPRITE

017-1,164,226,0	duplicate(#JumpArea01)	#JumpArea13	NPC_NO_SPRITE
017-1,164,227,0	duplicate(#JumpArea01)	#JumpArea14	NPC_NO_SPRITE
017-1,164,228,0	duplicate(#JumpArea01)	#JumpArea15	NPC_NO_SPRITE
017-1,164,229,0	duplicate(#JumpArea01)	#JumpArea16	NPC_NO_SPRITE
017-1,164,230,0	duplicate(#JumpArea01)	#JumpArea17	NPC_NO_SPRITE
017-1,164,231,0	duplicate(#JumpArea01)	#JumpArea18	NPC_NO_SPRITE

017-1,165,226,0	duplicate(#JumpArea01)	#JumpArea19	NPC_NO_SPRITE
017-1,165,227,0	duplicate(#JumpArea01)	#JumpArea20	NPC_NO_SPRITE
017-1,165,228,0	duplicate(#JumpArea01)	#JumpArea21	NPC_NO_SPRITE
017-1,165,229,0	duplicate(#JumpArea01)	#JumpArea22	NPC_NO_SPRITE
017-1,165,230,0	duplicate(#JumpArea01)	#JumpArea23	NPC_NO_SPRITE
017-1,165,231,0	duplicate(#JumpArea01)	#JumpArea24	NPC_NO_SPRITE

