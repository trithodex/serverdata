// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  Controls the swimming minigame. Basically, you lose 1% HP if lack total status.

018-5-0,0,0,0	script	#01850SwimmingCtrl	NPC_HIDDEN,{
    end;

OnLoop:
    if (getmap() != "018-5-0")
        end;
    if (ispcdead())
        end;
    // Sum everything - you can have 70 str instead of splitting in 35 str and 35 vit
    .@status=readparam2(bStr)+readparam2(bVit);
    .@reqst=.reqstr+.reqvit;
    debugmes "Got %d/%d (%d), heal %d", .@status,.@reqst,.@status-.@reqst,min(0, .@status-.@reqst);
    //percentheal min(0, .@status-.@reqst), 0;
    heal (min(0, .@status-.@reqst)*3), 0;
    addtimer(3000, "#01850SwimmingCtrl::OnLoop");
    end;

OnInit:
    .reqstr=35;
    .reqvit=35;
    end;
}
