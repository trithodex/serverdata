// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Nivalis shopkeeper & forge master. He deals with the equipment which Nicholas,
//    Silversmith and Nahred doesn't works with.
// TODO FIXME: Rewrite Meltdown to don't allow if countitem(id) > 1
// Also, use deletion by ID (reliable).
// delitemidx is HOPELESSY BROKEN

020-5,31,25,0	script	Bracco	NPC_M_SHOPKEEPER,{
    goto L_Start;
    // Meltdown( item, price, {id1, amount1}, {id2, amount2}... )
    function Meltdown {
        if (getargcount() < 2 || getargcount() % 2 != 0)
            return Exception("Faulty learning Meltdown command invoked - error");
        if (countitem(getarg(0)) != 1) {
            mesc l("Wait, if you try to melt more than one item, manaplus will get buggy."), 1;
            mesc l("Please try again later!"), 1;
            close;
        }

        .@index=getarg(0);
        .@price=getarg(1);
		.@price=POL_AdjustPrice(.@price);

        // Confirmation
        mesn;
        mesc l("Really melt down your @@? It'll cost you @@ GP. This action cannot be undone!", getitemlink(.@index), .@price), 1;
        next;
        if (askyesno() == ASK_NO || Zeny < .@price)
            return;

        // Report it was done
        mesc l("@@ melt down your @@...", .name$, getitemlink(.@index));

        delitem .@index, 1;
		POL_PlayerMoney(.@price);

        // TODO: Inventoryplace.
        // Add Items (if inventory is full, your fault and not mine)
        for (.@i=2;.@i < getargcount(); .@i++) {
            if (getarg(.@i+1)) {// It may be zero
                getitem getarg(.@i), getarg(.@i+1);
                mesc l("* Acquired @@ @@!", getarg(.@i+1), getitemlink(getarg(.@i)));
            }
            .@i++;
        }
        @indexisbroken=true;
        return;
    }

    // MassMeltdown( item, price, {id1, amount1}, {id2, amount2}... )
    function MassMeltdown {
        if (getargcount() < 2 || getargcount() % 2 != 0)
            return Exception("Faulty learning Meltdown command invoked - error");

        .@id=getarg(0);
        .@price=getarg(1);
        .@total=countitem(.@id);
        if (!.@total) {
            mesc l("You don't have any %s.", getitemlink(.@id)), 1;
            mesc l("Please try again later!"), 1;
            next;
            return;
        }

		.@price=POL_AdjustPrice(.@price);

        // Skip Confirmation
        mesn;
        mesc l("Really melt down all your @@? It'll cost you @@ GP each. This action cannot be undone!", getitemlink(.@id), .@price), 1;
        next;
        if (askyesno() == ASK_NO || Zeny < .@price)
            return;

        delinventorylist();
        getinventorylist();

        delitem .@id, .@total; // Delete first, no refunds

        for (.@index=0; .@index < @inventorylist_count; .@index++) {
            .@x=@inventorylist_id[.@index];
            if (.@x == getarg(0) && Zeny >= .@price) {
                //delitemidx .@index, 1;
				POL_PlayerMoney(.@price);
                // Report it was done
                mesc l("@@ melt down your @@...", .name$, getitemlink(.@x));

                for (.@i=2;.@i < getargcount(); .@i++) {
					.@v=getarg(.@i+1)+any(-1, 0, 0, 0, 1);
                    if (.@v > 0) {// It may be zero
                        getitem getarg(.@i), .@v;
                        mesc l("* Acquired @@ @@!", .@v, getitemlink(getarg(.@i)));
                    }
                    .@i++;
                }
            }
        }
        @indexisbroken=true;
        return;
    }

    // blacksmith_create( BaseItem1, Amount, BaseItem2, Amount, PrizeItem, Price )
    function blacksmith_create {
        .@base1=getarg(0);
        .@amon1=getarg(1);
        .@base2=getarg(2);
        .@amon2=getarg(3);
        .@prize=getarg(4);
        .@price=getarg(5);

		.@price=POL_AdjustPrice(.@price);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        mesc l("@@/@@ GP", format_number(Zeny), format_number(.@price));

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            countitem(.@base2) >= .@amon2 &&
            Zeny >= .@price) {
            inventoryplace .@prize, 1;
            delitem .@base1, .@amon1;
            delitem .@base2, .@amon2;
			POL_PlayerMoney(.@price);
            getitem .@prize, 1;
            .@xp=getiteminfo(.@base1, ITEMINFO_SELLPRICE)*.@amon1+getiteminfo(.@base2, ITEMINFO_SELLPRICE)*.@amon2;
            .@xp=.@xp*2/3;
            getexp .@xp, rand(1,10);

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;
    }

// Start
L_Start:
    mesn;
    mesq l("Welcome to my fine establishment!");
    mes "";
    select
        l("Trade"),
        l("I'm actually looking for an item forged!"),
        l("I would like an item melted!"),
        l("I would like all Knifes and Daggers on me melted!"),
        l("Leave");
    mes "";

    if (@menu == 2)
        goto L_Forge;

    if (@menu == 3)
        goto L_Meltdown;

    if (@menu == 4)
        goto L_Irreversible;

    closedialog;
    if (@menu == 1) {
        npcshopattach(.name$);
        shop .name$;
    }
    goodbye;
    close;
// Note: the prices are absurd atm, but hey hey, every single one of them are cap items currently
L_Forge:
    mesn;
    mesq l("Well, if you want warrior craft, perhaps you should look for @@ or @@.", l("Nicholas"), l("Nahrec"));
    mes "";
    select
        l("Nothing, sorry!"),
        l("I want leather armbands!"),
        l("I want copper armbands!"),
        l("I want iron armbands!");
    mes "";
    switch (@menu) {
        case 1:
            close; break;
        case 2:
            blacksmith_create(LeatherPatch, 40, TitaniumIngot, 1, Armbands, 6500);
            break;
        case 3:
            blacksmith_create(CopperIngot, 10, Coal, 30, CopperArmbands, 11000);
            break;
        case 4:
            blacksmith_create(IronIngot, 40, Coal, 80, IronArmbands, 21000);
            break;
    }
    goto L_Forge;

L_Irreversible:
    mesn;
    mesq l("Quite the guts! The price is taxed individually, if you run out of GP it is your loss.");
    mesc l("Are you sure?"), 1;
    next;
    menuint
        l("I'm not."), 0,
        l("Rusty Knife"), RustyKnife,
        l("Small Knife"), SmallKnife,
        l("Knife"), Knife,
        l("Sharp Knife"), SharpKnife,
        l("Dagger"), Dagger;
    mes "";
    .@it=@menuret;
    switch (@menuret) {
    // Copy Paste from normal Meltdown
    case RustyKnife:
        MassMeltdown(.@it, 15, IronOre, any(0, 0, 0, 1, 1));
        break;
    case SmallKnife:
        MassMeltdown(.@it, 15, IronOre, any(0, 0, 1, 1, 1));
        break;
    case Knife:
        MassMeltdown(.@it, 25, IronOre, any(0, 1, 1, 2));
        break;
    case SharpKnife:
        MassMeltdown(.@it, 50, IronOre, any(1, 2, 2, 3));
        break;
    case Dagger:
        MassMeltdown(.@it, 100, IronOre, any(2, 2, 3, 3, 4));
        break;
    }
    close;




L_Meltdown:
    mesn;
    mesc l("What item do you want to melt down? This is irreversible, and may return some ingots to you, but there is no way to tell how many you'll receive!"), 1;
    mesc l("Each item have it's own tax.");
    .@id=requestitem();
    if (.@id <= 0)
        close;
    mes "";
    // Returns 50~70% of invested ingots, rounded down. Never returns Coal.
    switch (.@id) {
    // Special Exceptions
    case SilverMirror:
        Meltdown(.@id, 500, SilverOre, rand2(2, 5)); // Exception
        break;
    case RustyKnife:
        Meltdown(.@id, 15, IronOre, any(0, 0, 0, 1, 1)); // Exception
        break;
    case SmallKnife:
        Meltdown(.@id, 15, IronOre, any(0, 0, 1, 1, 1)); // Exception
        break;
    case Knife:
        Meltdown(.@id, 25, IronOre, any(0, 1, 1, 2)); // Exception
        break;
    case SharpKnife:
        Meltdown(.@id, 50, IronOre, any(1, 2, 2, 3)); // Exception
        break;
    case Dagger:
        Meltdown(.@id, 100, IronOre, any(2, 2, 3, 3, 4, 5)); // Exception
        break;
    // Official Weapons
    case WoodenSword:
        Meltdown(.@id, 500, WoodenLog, rand2(5,10)); // Exception: 25~50% returned
        break;
    case BugSlayer:
        Meltdown(.@id, 1000, IronIngot, rand2(4,5));
        break;
    case ShortGladius:
        Meltdown(.@id, 1500, IronIngot, rand2(6,8));
        break;
    case Backsword:
        Meltdown(.@id, 2000, IronIngot, rand2(9,12), TinIngot, rand2(1,2));
        break;
    case ShortSword:
        Meltdown(.@id, 2500, IronIngot, rand2(12,16), TinIngot, rand2(2,3));
        break;
    case Kitana:
        Meltdown(.@id, 2500, IronIngot, rand2(15,21), TinIngot, rand2(4,6));
        break;
    case BoneKnife:
        Meltdown(.@id, 3000, IronIngot, rand2(18,25), Bone, rand2(45,62));
        break;
    case LongSword:
        Meltdown(.@id, 3000, IronIngot, rand2(21,29), IridiumIngot, rand2(0,1));
        break;
    case RockKnife:
        Meltdown(.@id, 3000, TerraniteOre, rand2(25,35), IridiumIngot, 1);
        break;
    case DivineSword:
        Meltdown(.@id, 3000, DivineApple, 1, PlatinumIngot, rand2(1,2), IridiumIngot, rand2(2,3));
        break;
    // 2 hand swords
    case MiereCleaver:
        Meltdown(.@id, 2000, SilverIngot, rand2(6,8));
        break;
    case Broadsword:
        Meltdown(.@id, 2000, SilverIngot, rand2(13,18));
        break;
    case Halberd:
        Meltdown(.@id, 2000, SilverIngot, rand2(22,31), TinIngot, rand2(2,3));
        break;
    case ImmortalSword:
        Meltdown(.@id, 2000, SilverIngot, rand2(20,28), IridiumIngot, 1);
        break;
    // Bows can go up to 100% but only wood is returned
    case ShortBow:
        Meltdown(.@id, 2000, WoodenLog, rand2(9,18));
        break;
    case ForestBow:
        Meltdown(.@id, 2000, WoodenLog, rand2(12,24));
        break;
    case ElficBow:
        Meltdown(.@id, 1500, WoodenLog, rand2(16,33));
        break;
    case ChampionshipBow:
        Meltdown(.@id, 1000, WoodenLog, rand2(24,48));
        break;
    case BansheeBow:
        Meltdown(.@id, 500, WoodenLog, rand2(35,70));
        break;

    // Wands have no warranted min. and are more expensive because they are sold
    case TrainingWand:
        Meltdown(.@id, 12000, WoodenLog, rand2(1,14), ManaPiouFeathers, rand2(0, 21), FluoPowder, rand2(0,3));
        break;
    case NoviceWand:
        Meltdown(.@id, 12000, WoodenLog, rand2(2,28), ManaPiouFeathers, rand2(1, 42), FluoPowder, rand2(0,5));
        break;
    case ApprenticeWand:
        Meltdown(.@id, 12000, WoodenLog, rand2(3,42), ManaPiouFeathers, rand2(2, 63), FluoPowder, rand2(0,8));
        break;
    case LeaderWand:
        Meltdown(.@id, 12000, WoodenLog, rand2(5,70), ManaPiouFeathers, rand2(3, 84), FluoPowder, rand2(2,10));
        break;
    case LegendaryWand:
        Meltdown(.@id, 12000, WoodenLog, rand2(7,77), GoldenApple, rand2(0, 1), FluoPowder, rand2(4,14));
        break;
    // Shields - same rule (except leather shield)
    case RoundLeatherShield:
        Meltdown(.@id, 500, LeatherPatch, rand2(0,1));
        break;
    case LeatherShield:
        Meltdown(.@id, 500, LeatherPatch, rand2(1,3));
        break;
    case WoodenShield:
        Meltdown(.@id, 1500, WoodenLog, rand2(20,28), LeatherPatch, 1);
        break;
    case BladeShield:
        Meltdown(.@id, 1500, IronIngot, rand2(7,9), TitaniumIngot, 1);
        break;
    case BraknarShield:
        Meltdown(.@id, 1500, CopperIngot, rand2(9,12), TinIngot, 1);
        break;
    // Etc
    case GoldenRing:
        Meltdown(.@id, 1500, GoldPieces, rand2(2,3));
        break;
    default:
        mesn;
        mesq l("I cannot melt this. I only melt down equipment, and not everything I know how to!");
        next;
        break;
    }
    mesc l("Melt something else?");
    if (askyesno() == ASK_NO)
        close;
    mes "";
    goto L_Meltdown;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, TneckSweater);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, FurBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 2);

    tradertype(NST_MARKET);
    sellitem LeatherShirt, 15000, 1;
    sellitem LeatherShield, 5000, 1;
    sellitem ShortBow, 11200, 1;
	sellitem ArrowAmmoBox,-1,rand(8,12);
	sellitem IronAmmoBox,-1,rand(3,5);

    npcsit;
    .sex = G_MALE;
    .distance = 5;
    end;

OnWed0000:
OnThu0400:
OnFri0800:
OnSat1200:
OnSun1600:
OnMon2000:
    restoreshopitem LeatherShirt, 15000, 1;
    restoreshopitem LeatherShield, 5000, 1;
    restoreshopitem ShortBow, 11200, 1;
	restoreshopitem ArrowAmmoBox,rand(8,12);
	restoreshopitem IronAmmoBox,rand(3,5);
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;

}
