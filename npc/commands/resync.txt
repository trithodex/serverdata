// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Introduces @resync
//
// It'll cast slide to your own position
// Hopefully making client update your real position without causing server warning
//
// This also introduces @resyncall
// Which is an alias for @refresh and causes client to reload the whole map,
// Including yourself and monsters.

-	script	@resync	32767,{
    end;

// Soft Resync
OnCall:
    if (ispcdead()) {
        dispbottom l("Impossible to resync: You are dead.");
        end;
    }
    if (@rsync_delay > gettimetick(2)) {
        dispbottom l("Not resync'ing to prevent flood.");
        end;
    }
    getmapxy(.@m$, .@x, .@y, 0);
    slide .@x, .@y;
    @rsync_delay=gettimetick(2)+rand2(3,5);
    end;

// Hard Resync
OnCallRefresh:
    if (@rsync_delay > gettimetick(2)) {
        dispbottom l("Not resync'ing to prevent flood.");
        end;
    }
    @rsync_delay=gettimetick(2)+rand2(3,5);
    atcommand("@refresh");
    end;

OnInit:
    bindatcmd "resync", "@resync::OnCall", 0, 60, 0;
    bindatcmd "resyncall", "@resync::OnCallRefresh", 0, 60, 0;
    end;
}
