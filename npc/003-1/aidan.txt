// TMW-2 Script
// Author: Crazyfefe, Jesusalva
// Desc:   Originally a Tmw script

003-1,95,97,0	script	Aidan	NPC_PLAYER,{

    function Register
    {
        mesn l("Aidan, the Monster Guide");
        mesq l("Oh my, you don't seem to be registered as a Monster Hunting Quest Participant. Would you like to register?");
        next;
        mesn l("Aidan, the Monster Guide");
        mesq l("The register fee is 2000 GP.");

        do
        {
            select
            rif(Zeny >= 2000, l("Register")),
            rif(Zeny < 2000, l("How do I get so much money?!")),
            l("Not at the moment"),
            l("Information");

        switch (@menu) {
            case 1:
                mes "";
                Zeny=Zeny-2000;
                MPQUEST=1;
                mesn l("Aidan, the Monster Guide");
                mesq l("Give me a second to look over your paperwork.");
                next;
                mesn l("Aidan, the Monster Guide");
                mes l("\"Well, looks like you qualify!");
                mes l("Welcome to the questing world!\"");
                close;
                break;
            case 2:
                mes "";
                mesn l("Aidan, the Monster Guide");
                mesq l("Sell old equipment and items you won't use. For example, what should you do with a @@ or an @@? Sell it!", getitemlink(Ruby), getitemlink(ScorpionStinger));
                close;
                break;
            case 3:
                mes "";
                mesn l("Aidan, the Monster Guide");
                mesq l("Very well, you don't know what you're missing.");
                close;
                break;
            case 4:
                mes "";
                mesn l("Aidan, the Monster Guide");
                mesq l("You see, because the Monster King, monsters have been running rampant. If they grow too much in numbers, cities may be overrun.");
                next;
                mesn l("Aidan, the Monster Guide");
                mesq l("Therefore, the Alliance created a system so when you kill a monster, depending on its strength, you'll get Monster Points.");
                next;
                mesn l("Aidan, the Monster Guide");
                mesq l("To prevent abuse, a registering fee is charged. Nothing major.");
                next;
                mesn l("Aidan, the Monster Guide");
                mesq l("So whaddaya say, sign up won't you?");
                next;
                mes "";
                Register;
                break;
        }
        } while (@menu != 4);
    }

    if (BaseLevel < 10) goto L_Weak;

    if (MPQUEST == 0)
        Register;

    mesn l("Aidan, the Monster Guide");
    mesq l("You currently have @@ Monster Points. These points are acquired while killing monsters.", Mobpt);
    if (getq(General_Hunter) == 0 && !GHQUEST) goto L_Register;
    if (getq(General_Hunter) == 0) goto L_Assign;
    mes "";
    goto L_Assign;
    close; // Will never be reach.

L_Weak:
    mesn;
    mesq l("How did you even get here? Go back to Candor, where you belong!");
    percentheal -20, 0;
    close;

L_Register:
    next;
    mesn;
    mesq l("The alliance also have a special program, called ##BGrand Hunter Quest##b, where you kill 10,000 of a monster and get great rewards.");
    next;
    mesn;
    mesq l("You can gain rares, even. Come register for this special program. It's free!");
    if (askyesno() == ASK_YES) {
        GHQUEST=1;
        setarray GHMEMO, 0, 0, 0;
        mesn;
        mesq l("Registered, welcome to the Grand Hunter Quest!");
        next;
        goto L_Assign;
    } else {
        mes "";
        mesn;
        mesq l("A pity...");
        close;
    }

L_Assign:
    GHQ_Assign(Maggot, "Tulimshar");
    end;


OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 3);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 3);

    .sex = G_MALE;
    .distance = 5;
    end;
}


