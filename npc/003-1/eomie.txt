// TMW2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Part of Anwar Field quest
// Notes: Eomie did the bug bomb at Candor

003-1,68,24,0	script	Eomie	NPC_ELF_F,{
    .@q=getq(TulimsharQuest_AnwarField);
    if (.@q == 10) goto L_Gift;
    if (.@q == 7) goto L_FirstAid;
    if (.@q == 1) goto L_NotMe;

    hello;
    end;

L_NotMe:
    mesn strcharinfo(0);
    mesq l("Hello Ms. Eomie, kind sir Anwar sent me to fetch some fertilizers to save Tulimshar from famine, if you may?");
    next;
    mesn;
    mesq lg("Sorry kind lady @@, but no.", "Sorry kind sir @@, but no.", strcharinfo(0));
    next;
    mesn;
    mesq l("Or rather, I can't. I would love to help you, just like everybody else, but I don't know how to make fertilizers.");
    next;
    mesn;
    mesq l("Tinris probably could do that, he is young but very talented. He is a greedy elf, but if you help him, he'll likely help you back.");
    setq TulimsharQuest_AnwarField, 2;
    close;

L_FirstAid:
    mesn;
    mesq l("The crops are under attack? That's terrible!");
    next;
    mesn;
    mesq l("I can do a bug bomb right away, but I still need a few things for it!");
    next;
    mesn;
    mesq l("Do you, perchance, have 2 @@ and 3 @@?", getitemlink(ScorpionClaw), getitemlink(Moss));
    if (askyesno() != ASK_YES)
        close;
    mes "";

    if (countitem(ScorpionClaw) < 2 ||
        countitem(Moss) < 3) {
        mesn;
        mesq l("The situation is too serious to you be lying... Please, go fetch the items...");
        close;
    }

    delitem ScorpionClaw, 2;
    delitem Moss, 3;
    setq TulimsharQuest_AnwarField, 8;

    mesn;
    mesq l("Quick, deliver this to Anwar!");
    close;


L_Gift:
    .@q2=getq2(TulimsharQuest_AnwarField);
    if (.@q2 & 2) {
        mesn;
        mesq l("Thanks for the nice gift!");
        close;
    }
    // Tip. WHAT DID YOU DID WITH THE BOUND ITEM? IT SHOULD BE HARD TO GET RID OF IT...
    if (countitem(TortugaShell) < 1) {
        mesn;
        mesq l("Ah, I wish I got something for helping people out...");
        close;
    }
    mesn strcharinfo(0);
    mesq l("Anwar sent you this, erm, hum... @@.", getitemlink(TortugaShell));
    next;
    setq2 TulimsharQuest_AnwarField, .@q2+2;
    delitem TortugaShell, 1;
    getexp 75, 10;
    mesn;
    mesq l("WOW, THIS IS AWESOME! Many, many thanks!!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, PinkieHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ValentineDress);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    //setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 12);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
