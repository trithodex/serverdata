// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    The Travelers travel around the world telling stories.

003-1,56,143,0	script	Nina The Traveler	NPC_F_COINKEEPER,{

    mesn;
    if (strcharinfo(0) == $MOST_HEROIC$) mesq l("Wow! Are you @@? Everyone, in every city, talks about you!", $MOST_HEROIC$);
    if (strcharinfo(0) == $MOST_HEROIC$) next;

    mesq l("Hello. I am @@, and I am from a family of travellers. We travel though the whole world, looking for exotic goods.", .name$);
    next;
    mesq l("You can buy rare items with me, or I can tell you about different cities in our world.");

L_Menu:
    mes "";
    menu
        l("I want to trade with you."), L_Trade,
        l("Tell me about Tulimshar."),  L_Tulim,
        l("Tell me about Hurnscald."),  L_Hurns,
        l("Tell me about Artis."),      L_Artis,
        l("Tell me about Halinarzo."),  L_Halin,
        l("Sorry, I'll pass."), L_Close;

L_Tulim:
    mes "";
    mesn;
    mesq l("Tulimshar is the oldest human city, and its foundation is the year zero of our calendar.");
    next;
    mesq l("The city only flourished because Janett Platinum had the idea to build city walls surrounding this city.");
    next;
    mesq l("The desert climate means you'll find mostly maggots and scorpions. Their drops include cactus drinks, cake, knifes, black pearls, gold, and other common things.");
    next;
    mesq l("You can find for a good price desert equipment and some kind of dyes. You find all sort of crafters, artisans and warriors here.");
    next;
    goto L_Menu;

L_Hurns:
    mes "";
    mesn;
    mesq l("Hurnscald was founded after Tulimshar, in more fertile lands. Their walls are not so sturdy as the ones of Tulimshar.");
    next;
    mesq l("Under the leadership of King Wusher, they were the first to accept immigrants from other races. You will find humans and non-humans there.");
    next;
    mesq l("The fertile climate is ideal for mushrooms. You can also find lots of wood.");
    next;
    mesq l("Their economy provide many edible items and potions.");
    next;
    goto L_Menu;

L_Artis:
    mes "";
    mesn;
    mesq l("Artis is a city port founded after the Great Famine on the other continent.");
    next;
    mesq l("People say it is the second biggest city from the world.");
    next;
    mesq l("Different kind of monsters live near the city. For example, blubs. I have no idea of what are those.");
    next;
    mesq l("People usually dock there when travelling to the second continent. Nothing exceptional about economy.");
    next;
    goto L_Menu;

L_Halin:
    mes "";
    mesn;
    mesq l("Halinarzo was founded to explore Mana Stones.");
    next;
    mesq l("You can find both huge swamps, as huge desertic areas near and on it.");
    next;
    mesq l("Lizards are the main monster found, and they steal gold from innocent bypassers.");
    next;
    mesq l("Without any mana stone left, and because the walls were not very strong, most of the city was destroyed.");
    next;
    mesq l("Unlike many other cities, if you want people in eternal need of items, there is a good place to look.");
    next;
    goto L_Menu;


L_Trade:
    mesn;
    mesq l("Use your @@ as currency!", getitemlink(StrangeCoin));
    tutmes l("%s is obtained during events, daily logins, heroic deeds, gifts, etc. But cannot be bought with real money.", getitemlink(StrangeCoin));
    next;
    openshop "Aeros Trader";
    closedialog;

L_Close:
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, UglyChristmasSweater);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);
    setunitdata(.@npcId, UDT_WEAPON, CandorBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 27);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 11);
    npcsit;

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

