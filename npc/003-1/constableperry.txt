// TMW2 Scripts.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Constable Perry invites players to the TMW2 Project
//    Part of the THIEF/MERCHANT branches

003-1,47,75,0	script	Constable Perry	NPC_MOUBOO,{
    if (getequipid(EQI_HEAD_TOP) == AFKCap) goto L_Quest;

L_Intro:
    mesn;
    mesq l("Hello there! I am a constable. I keep law and order here.");
    next;
    mesq l("Yes, I am a mouboo. Why? Can't a mouboo be a law and order enforcer?!");
    next;
    mesq l("Have you ever felt struck? Lost? Didn't know about a quest, or an item is troubling you?");
    mesq l("Have no idea where in the world you are? Or what a certain mob drops, or even if you should challenge it?");
    next;
    mesn;
    mesq l("Fear not! You can check our [@@https://wiki.moubootaurlegends.org|Wiki@@] to find that and other awesome stuff!");
    //mesq l("Fear not! You can check our [@@https://gitlab.com/TMW2/Docs/wikis/home|Wiki@@] to find that and other awesome stuff!");
    mesc l("(To see the rules, use ##B@rules##b.)");
    next;
    mesq l("You can even join the project there. Contributors are greatly appreciated! %%N");
    close;

L_Quest:
    // Quest Requeriment of 48 hours or idle before speaking for 60 min
    if (AFKING < 57600 && checkidle() < 3600) {
        npctalkonce l("What's that? He seems to be AFK but is not?");
        if (.@touchevent)
            end;
        else
            goto L_Intro;
    }
    // No warning given
    if (!@afkbotwarned) {
        mesn;
        mesq l("Stop right there, meliant. I can see you're AFK, it is written in your hat!");
        next;
        mesn;
        mesq l("Moving while AFK is botting! I'll send you to a %s if you move while AFK! YOU HAVE BEEN WARNED!", b(l("cold, hard, iron cell")));
        @afkbotwarned=true;
        close;
    }
    // Now you've done it!
    if (@afkbotwarned)
        goto L_NowYouHaveDoneIt;
    close;

L_NowYouHaveDoneIt:
    mesn;
    mesq l("NOW YOU HAVE DONE IT! By the powers to me vested as a Constable and Game Master, I sentence you to HALF HOUR IN A %s!", b(strtoupper(l("cold, hard, iron cell"))));
    next;
    mesn;
    mesq l("Do you have any last wishes before being JAILED for your CRIMES?!");
    next;
    select
        l("Please tell my mom I love her!"),
        l("Pinkies forever! Oh yeah!"),
        l("I did nothing wrong! I am not AFK!"),
        l("Can I have a Blanket, at least?"),
        l("Please tell everyone I am a rogue person who likes to break rules!"),
        l("I am a thief! I want to repent for my sins!"),
        l("No, sir.");
    mes "";
    mesn;
    switch (@menu) {
        case 2:
            mesq l("Hmph, you are the second activist I see today! TO THE BRIG!");
            break;
        case 3:
            mesq l("Every bot says that.");
            break;
        case 4:
        	.@q=getq(FrostiaQuest_AFKCap);
            if (.@q == 2) {
                mesq l("Uh, sure. We mouboos are not cruel. But it might not be your size.");
                getitem Blanket, 1;
                setq1 FrostiaQuest_AFKCap, 3;
            } else {
                mesq l("This is not the first time you're arrested for AFK botting, so, NO!");
            }
            break;
        case 5:
            mesq l("Hmph, that would only incite bad behavior! No way!");
            break;
        case 6:
            mesq l("You have no proof that you have robbed vaults or sided with Ben Parkison, the Thieves Guild Master.");
            break;
        default:
            mesq l("...");
            break;
    }
    atcommand("@jailfor 30mn "+strcharinfo(0));
    dispbottom l("Use %s to see how long you need to wait.", b("@jailtime"));
    close;

OnTouch:
    .@touchevent=true;
    if (getequipid(EQI_HEAD_TOP) == AFKCap)
        goto L_Quest;
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

