// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Grenades workaround

// grenade(range, damage - in 0.01%, flag) - defaults to 3x3 square, with 5% damage.
// If flag is set, damage will be deemed to be absolute values.
function	script	grenade	{
    .@r=getarg(0, 3);
    .@d=getarg(1, 500);
    .@f$=getarg(2, "filter_notboss");

    getmapxy(.@m$, .@x, .@y, 0);
    .@c=getunits(BL_MOB, .@mbs, false, .@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
    for (.@i = 0; .@i < .@c; .@i++) {
        .@hp=getunitdata(.@mbs[.@i], UDT_HP);
        .@dm=max(1, .@hp*(10000-.@d)/10000);
        if (getarg(2, false))
            .@dm=max(1, .@hp-.@d);
        if (callfunc(.@f$)) {
            //debugmes "Hitting monster (%d hp) for %d damage", .@hp, .@dm;
            setunitdata(.@mbs[.@i], UDT_HP, .@dm);
            specialeffect(FX_ATTACK, AREA, .@mbs[.@i]);
        }
    }
    return;
}

// areasc(range, time, sc, bl, value, filter, target, chances)
// Defaults to 3x3 square, sleep mob for 500ms. Ignores you.
// Centered on player attached, 100% success chance
// Need a player caster. Valid BL: BL_MOB | BL_PC | BL_HOM | BL_MER
function	script	areasc	{
    .@r=getarg(0, 3);
    .@d=getarg(1, 500);
    .@s=getarg(2, SC_SLEEP);
    .@b=getarg(3, BL_MOB);
    .@val=getarg(4, 1);
    .@f$=getarg(5, "filter_notme");
    .@t=getarg(6, playerattached());
    .@sr=getarg(7, 10000);

    getmapxy(.@m$, .@x, .@y, getunittype(.@t), .@t);
    .@c=getunits(.@b, .@mbs, false, .@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
    for (.@i = 0; .@i < .@c; .@i++) {
        // Filtering
        if (!callfunc(.@f$, .@mbs[.@i]))
            continue;
        sc_start .@s, .@d, .@val, .@sr, SCFLAG_NONE, .@mbs[.@i];
        specialeffect(FX_BUFF, AREA, .@mbs[.@i]);
    }
    return;
}

// areasc2(map, x, y, {range, time, sc, bl, value, filter}) - can be used by NPC
// Valid BL: BL_MOB | BL_PC | BL_HOM | BL_MER
function	script	areasc2	{
    .@m$=getarg(0);
    .@x=getarg(1);
    .@y=getarg(2);
    .@r=getarg(3, 3);
    .@d=getarg(4, 500);
    .@s=getarg(5, SC_SLEEP);
    .@b=getarg(6, BL_MOB);
    .@val=getarg(7, 1);
    .@f$=getarg(8, "filter_always");

    .@c=getunits(.@b, .@mbs, false, .@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
    for (.@i = 0; .@i < .@c; .@i++) {
        // Filtering
        if (!callfunc(.@f$, .@mbs[.@i]))
            continue;
        sc_start .@s, .@d, .@val, 10000, SCFLAG_NONE, .@mbs[.@i];
        specialeffect(FX_BUFF, AREA, .@mbs[.@i]);
    }
    return;
}

// areasc3(range, time, sc, bl, val1, val2, filter)
// Defaults to 3x3 square, sleep mob for 500ms. Ignores you.
// Need a player caster. Valid BL: BL_MOB | BL_PC | BL_HOM | BL_MER
function	script	areasc3	{
    .@r=getarg(0, 3);
    .@d=getarg(1, 500);
    .@s=getarg(2, SC_SLEEP);
    .@b=getarg(3, BL_MOB);
    .@v1=getarg(4, 1);
    .@v2=getarg(5, 1);
    .@f$=getarg(6, "filter_notme");

    getmapxy(.@m$, .@x, .@y, 0);
    .@c=getunits(.@b, .@mbs, false, .@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
    for (.@i = 0; .@i < .@c; .@i++) {
        // Filtering
        if (!callfunc(.@f$, .@mbs[.@i]))
            continue;
        sc_start2 .@s, .@d, .@v1, .@v2, 10000, SCFLAG_NONE, .@mbs[.@i];
        specialeffect(FX_BUFF, AREA, .@mbs[.@i]);
    }
    return;
}

// massprovoke(range, {map, x, y}) - player only
function	script	massprovoke	{
    getmapxy(.@m$, .@x, .@y, 0);
    .@r=getarg(0, 3);
    .@m$=getarg(1, .@m$);
    .@x=getarg(2, .@x);
    .@y=getarg(3, .@y);

    .@c=getunits(BL_MOB, .@mbs, false, .@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
    for (.@i = 0; .@i < .@c; .@i++) {
        //sc_start .@s, .@d, 1, 10000, SCFLAG_NONE, .@mbs[.@i];
        aggravate .@mbs[.@i];
        specialeffect(FX_MAGIC, AREA, .@mbs[.@i]);
    }
    return;
}

// TODO: Maybe we could use areasc() with a special check
// To force the implementation of guild skills... (Yet another script based)
