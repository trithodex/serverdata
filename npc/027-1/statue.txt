// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    These statues are of great honor to whoever have their name written in them!

027-1,57,115,0	script	Hero Statue#027-1	NPC_STATUE_ANDREI,{

    mes l("In honor of %s, the greatest hero this world has even seen.", b("Andrei Sakar"));
    mes "";
    if ($MOST_HEROIC$ != "") {
        mes l("In honor of %s, for their great deeds of recent bravery in face of impending doom.", $MOST_HEROIC$);
        mes "";
    }
    mes l("In honor of %s, founder of this academy, whom built this academy WITH HIS OWN HANDS, WITH BLOOD SWEAT AND TEARS.", b("Jesusalva"));
    mes "";
    mes l("Also in honor of the other two sages, %s and %s. And finally, notable mention for the noble %s, %s and %s, for sponsoring this Academy.", b("Saulc"), b("Crazyfefe"), b("Micksha"), b("omatt"), b(rand_sponsor()));

    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

