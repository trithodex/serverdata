// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Permanently repeatable quest, without any special limit

017-3,68,87,0	script	Model	NPC_PLAYER,{
    mesn;
    mesq l("This room is too dark. I want to brighten it up.");
    next;
    .@price=(getiteminfo(CaveSnakeLamp, ITEMINFO_SELLPRICE)*11/10)*5;
    mesn;
    mesq l("I am willing to pay @@ GP for each 5 @@ you bring me!", .@price, getitemlink(CaveSnakeLamp));
    next;
    select
        rif(countitem(CaveSnakeLamp) >= 5, l("Here they are!")),
        l("Not now...");
    mes "";
    if (@menu == 1) {
        delitem CaveSnakeLamp, 5;
        Zeny=Zeny+.@price;
        getexp (.@price/rand(2,3)), 5;
        mesn;
        mesq l("Many, many thanks!");
        next;
    }
    mesn;
    mesq l("Too bad these lamps wear off after a while... I am making stocks of them now!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;
}

