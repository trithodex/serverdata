// TMW2 scripts.
// Authors:
//    Qwerty Dragon
//    Reid
//    Jesusalva
// Description:
//    Captain Nard dialogs.
//    Nard is a fair merchant ship's captain.
//    Original Nard's from Evol by Qwerty Dragon and Reid

002-3,36,25,0	script	Nard	NPC_NARD,{
    showavatar NPC_NARD;  // this is handled by avatars.xml

    .@narrator = getq(General_Narrator);

L_Checker:
    if (.@narrator) goto L_Travel;
    if (getq(ShipQuests_Julia) >= 3) goto L_NotYet;
    if (debug) goto L_TestServer;
    // Introduction
    mesn;
    mesq l("Hello.");
    next;
    mesq l("Let me introduce myself, I am Nard, captain of this ship.");
    next;
    mesq lg("I am pleased to see that you have woken up and are active. Elmo came here to tell me this good news!");
    next;
    mesq l("So, how do you feel? I see that Juliet did a marvellous job! You look like you're in good health now.");
    next;

    select
        lg("I feel ok."),
        l("Who's this Juliet?"),
        lg("I'm a bit sick...");
    mes "";
    switch (@menu) {
    case 1:
        mesn;
        mesq l("Good to know.");
        next;
        break;
    case 2:
        mesn;
        mesq lg("You have an awful case of amnesia. She is the nurse and shipkeeper of this ship, and took care of you when you were unconscious.");
        next;
        break;
    case 3:
        mesn;
        mesq l("Well, you'll need to get used to. Being seasick is annoying, so you might want to leave the ship as soon as possible.");
        next;
        break;
    }

L_Main:
    mesn;
    mesq l("We have made a stop at a little island, before making it on to the port of Tulimshar.");
    next;
    mesn;
    mesq l("Ship travels are not free. Also, I have a few friends on the Island, and I would like you to check out on them.");
    next;
    showavatar NPC_ELMO;
    setcamnpc instance_npcname("Elmo");
    mesn l("Elmo");
    mesq l("I, Elmo, captain's deputy, will help you to make the maximum possible money in Candor!");
    mesc l("Elmo has given you an EXP UP and DROP UP Boost until level 20!"), 2;
    mesc l("It also expires after two hours. In such case, talk to him again!"), 2;
    // Actually, why don't we apply it right now...?
    .@BONUS=NewcomerEXPDROPUP();
    specialeffect FX_SPECIAL, SELF, getcharid(3);
    mesc l("EXP Gain raised in @@% for one hour!", .@BONUS), 2;
    next;
    showavatar NPC_NARD;
    setcamnpc;
    mesn;
    mesq l("After that, we're going to Tulimshar. Tulim is the most important city on the world, and the Alliance have an office there.");
    next;
    mesn;
    mesq l("The Alliance can help you in finding out about who you are, why you are here, or from where you came from. So, about the tasks I want completed.");
    LOCATION$ = "Candor";
    setq ShipQuests_Julia, 3;
	// Event handling
	if ($EVENT$ == "Event")
		getitem MercCard_EH, 1;
    // Welcome handling
    if (!#REG_DATE) {
        $@WELCOME_TIMER=gettimetick(2)+900; // 15 minutes
        kamibroadcast("Hey, look, I've rescued \""+strcharinfo(0)+"\" from the sea! Who will @welcome them?!", "Nard");
        #REG_DATE=gettimetick(2);
    }
    next;
    .@price=800;
    mesc b(l(".:: Main Quest 1-3 ::.")), 3;
    msObjective(getq(CandorQuest_HAS) >= 4, l("* Help Ayasha to take care of the kids."));
    msObjective(getq(CandorQuest_Trainer) >= 12, l("* Get trained by Valon, in the big house."));
    msObjective(getq(CandorQuest_Barrel) >= 4, l("* Ask Zegas, the mayoress, if she needs help."));
    msObjective(Zeny >= .@price, l("* Collect @@/@@ GP", Zeny, .@price));

L_Referral:
    // Welcome handling
    if (!#REG_DATE) {
        $@WELCOME_TIMER=gettimetick(2)+600; // 10 minutes
        kamibroadcast("Hey, look, I've rescued \""+strcharinfo(0)+"\" from the sea! Who will @welcome them?!", "Nard");
        #REG_DATE=gettimetick(2);
    }
    // Referral program
    if (#REFERRAL_PROG == 0 && $REFERRAL_ENABLED) {
        next;
        clear;
        showavatar NPC_LOF_RICH;
        mesc l("But before, a message from our developers!"), 3;
        next;
        mesn l("TMW2 Staff");
        mesc l("Hello, and welcome to TMW2: Moubootaur Legends!"), 3;
        next;
        mesn l("TMW2 Staff");
        mesc l("Did you came here by someone advise? If yes, write their name down here!"), 3;
        next;
        mesc l("If this is not the case, just click on \"Send\"."), 3;
        .@ref$="";
        do
        {
            input .@ref$;
            //debugmes "Player invite: "+.@ref$;
            mes "";
            if (.@ref$ != "") {
                .@ref=gf_accid(strip(.@ref$));
                if (.@ref > 0) {
                    if (.@ref == getcharid(3)) {
                        mesn l("TMW2 Staff");
                        mesc l("Hahah, silly, that's yourself!"), 3;
                        mesc l("Try again!"), 3;
                        next;
                        .@ref$="";
                    } else {
                        #REFERRAL_PROG=.@ref;
                        getitembound FriendGift, 1, 1;
                        mesn l("TMW2 Staff");
                        mesc l("Well, welcome to the game! If you have any doubt, shout on #world for help!"), 3;
                        mesc l("Your friend also sent you a gift - open it when you get level 5!"), 3;
                        next;
                    }
                } else {
                    mesn l("TMW2 Staff");
                    mesc l("Oops, there is nobody known as @@ on this game.", .@ref$), 3;
                    mesc l("Could you try again? There could be a typo!"), 3;
                    next;
                    .@ref$="";
                }
            } else {
                .@ref$="None";
                mesn l("TMW2 Staff");
                mesc l("I see. Well, welcome to the game! If you have any doubt, shout on #world for help!"), 3;
                next;
            }
        } while (.@ref$ == "");
    showavatar NPC_NARD;
    }

    close;


L_NeedHelp:
    mes "";
    mesn;
    mesq l("You're pretty much stranded on this forsaken island if you don't help me!");
    next;
    mesq l("Also, I believe hard work always pay off.");
    next;
    goto L_NotYet;

L_CandorIsland:
    mes "";
    mesn;
    mesq l("I come here frequently to trade. It is not deserted nor boring.");
    next;
    mesq l("This is ##BCandor Island##b. A very small rich community lives here.");
    next;
    mesq l("If they were any bigger, monsters would come and kill everyone.");
    next;
    goto L_NotYet;

L_NotYet:
    .@price=800;
    mesc b(l(".:: Main Quest 1-1 ::.")), 3;
    msObjective(getq(CandorQuest_HAS) >= 4, l("* Help Ayasha to take care of the kids."));
    msObjective(getq(CandorQuest_Trainer) >= 12, l("* Get trained by Valon, in the big house."));
    msObjective(getq(CandorQuest_Barrel) >= 4, l("* Ask Zegas, the mayoress, if she needs help."));
    msObjective(Zeny >= .@price, l("* Collect @@/@@ GP", Zeny, .@price));
    mes "";
    select
        rif(Zeny >= .@price, l("I've brought the money you've asked for.")),
        rif(#REFERRAL_PROG == 0 && $REFERRAL_ENABLED && BaseLevel <= 10,
            l("I forgot to say earlier, but indeed, I was invited by someone!")),
        l("Captain, why have you brought me to a deserted boring island?!"),
        l("I don't want to help your \"friends\", bring me to somewhere useful!"),
        l("Please excuse me, captain.");

    mes "";
    if (@menu == 2)
        goto L_Referral;
    if (@menu == 3)
        goto L_CandorIsland;
    if (@menu == 4)
        goto L_NeedHelp;
    if (@menu == 5)
        close;

    mesn;
    if (TUTORIAL && (getq(CandorQuest_HAS)     < 4 ||
        getq(CandorQuest_Barrel)  < 4 ||
        getq(CandorQuest_Trainer) < 12)) {
        mesq l("You didn't help all my friends yet, and without trainment, I can't send you to such dangerous place as Tulimshar.");
        mesc l("NOTE: It is possible to play the game as a crafter/merchant/fisherman, avoiding to kill as much as possible. However, it is not possible to play the game with a total kill count of zero.");
        close;
    }
    // If you did less than 50% Candor quests, please be warned.
    if (reputation("Candor") < 50) {
        mesc l("WARNING: You have done less than 50% of Candor Quests!"), 1;
        mesc l("It may be expensive to return here. Are you sure?"), 1;
        next;
        if (askyesno() == ASK_NO)
            close;
    }
    if (Zeny >= .@price) {
        inventoryplace TulimMap, 1;
        mesq l("Ten, fifty, thousand... Yep, this is the amount I've asked for.");
        next;
        setq General_Narrator, 1;
        // Double sure
        setq ShipQuests_Julia, 3;
        Zeny = Zeny-.@price;
        EnterTown("Tulim");
        getitem TulimMap, 1;
        mesq l("Set sail! We're going to Tulimshar!");
        next;
        PC_DEST$="Tulim";
        addtimer nard_time(PC_DEST$)+800, .name$+"::OnNardStage";
        addtimer nard_time(PC_DEST$), "#NardShip::OnEvent";
        @timer_navio_running = 1;
        warp "002-5", 39, 26;
        closeclientdialog;
        end;
    } else {
        mesq l("You still haven't completed your tasks.");
        mes "";
        mesc l("You still need @@ GP for the trip to Tulimshar.", (.@price-Zeny));
    }
    close;

L_Travel:
    // Current nard_reputation() value for 100% discount: 15
    .@price=800;
    .@price-=min(780, nard_reputation()*54);

    mesn;
    mesq l("Hi @@.", strcharinfo(0));
    next;
    mesq l("You are currently at @@.", LOCATION$);
    mes "";
    mes l("A ship travel will cost you @@ GP.", .@price);
    if (ST_TIER == 8)
        mesc l("What are you doing? Go talk to @@  and bring me a @@!", b("Elanore"), getitemlink(Lifestone));

    if (Zeny >= .@price || ((ST_TIER == 7 || ST_TIER == 9) && gettimetick(2) < QUEST_ELEVARTEMPO)) {
        menu
            rif(Zeny >= .@price && LOCATION$ != "Candor", l("To Candor Island.")), L_TCandor,
            rif(Zeny >= .@price && LOCATION$ != "Tulim", l("To Tulimshar.")), L_TTulim,
            rif(ST_TIER == 7 && gettimetick(2) < QUEST_ELEVARTEMPO ,l("Help me, I need Jesusaves Grimorie!")), L_Tier2,
            rif(ST_TIER == 9 && countitem(Lifestone) && gettimetick(2) < QUEST_ELEVARTEMPO ,l("Help me, I need Jesusaves Grimorie!")), L_Tier2Ok,
            l("No, I'll save my money."), -;
    } else {
        mes l("You still need @@ GP to afford it.", (.@price-Zeny));
    }

    close;

L_TCandor:
    Zeny=Zeny-.@price;
    PC_DEST$="Candor";

    mes "";
    mesn;
    mesq l("Candor Island, then? Yes, that is a pretty island, right?");
    next;
    mesq l("I was planning to go there soon, anyway. All aboard!");
    close2;
    addtimer nard_time(PC_DEST$), "#NardShip::OnEvent";
    @timer_navio_running = 1;
    warp "002-5", 39, 26;
    end;

L_TTulim:
    Zeny=Zeny-.@price;
    PC_DEST$="Tulim";
    @timer_navio_running = 1;

    mes "";
    mesn;
    mesq l("Tulimshar, right? The oldest human city-state!");
    next;
    mesq l("I was planning to go there soon, anyway. All aboard!");
    close2;
    addtimer nard_time(PC_DEST$), "#NardShip::OnEvent";
    @timer_navio_running = 1;
    warp "002-5", 39, 26;
    end;

L_Tier2:
    mes "";
    mesn;
    mesq l("WHAT?! ARE YOU OUT OF MIND?!?!");
    next;
    mesn;
    if (nard_reputation() < 8) {
        mesq l("THAT GRIMORIE IS A SUPER DUPER MEGA UPER RARE BOOK, I CANNOT GIVE IT TO ANYBODY ASKING ME ABOUT!!");
        next;
        mesn;
        mesq l("GET OUT OF HERE, YOUR NOBODY!");
        close;
    }
    mesq l("That is a rare, precious book, which writes itself!");
    next;
    mesn;
    mesq l("I cannot just give it to you for nothing. Run to Elanore and fetch me a @@. You need to make a new one, an old one I won't accept.", getitemlink(Lifestone));
    ST_TIER=8;
    close;

L_Tier2Ok:
    mes "";
    mesn;
    mesc l("*tut*");
    next;
    inventoryplace JesusalvaGrimorium, 1;
    delitem Lifestone, 1;
    getitem JesusalvaGrimorium, 1;
    getexp 200, 0;
    ST_TIER=10;
    mesn;
    mesq l("Here, take it. If the mana goes out of your body, I'll have your class master to return the book to me.");
    close;

OnNardStage:
    showavatar NPC_NARD;  // this is handled by avatars.xml
    mesn;
    mesq l("Welcome to Tulimshar, @@!", strcharinfo(0));
    mesc l("Nard gives you a map of the city so you don't get lost.");
    next;
    if (GSET_SOULMENHIR_MANUAL) {
        mesn;
        mesq l("You can explore the city as you want, but if I were you, I would ##Btouch the Soul Menhir##b, north of here, to don't respawn at Candor.");
        next;
    } else {
        mesn;
        mesq l("If you die, you'll appear near Tulimshar's Soul Menhir, which is the nearest Menhir to you. You can use @@ if you ever want to change this behavior.", b("@ucp"));
        next;
    }
    mesn;
    mesq l("Other than that, you can explore the city as you want, but as you had a memory loss, You should visit the townhall.");
    next;
    mesn;
    mesq l("When you get out of the ship, it is the first building you'll see. Talk to ##BLua##b, she is an alliance representative.");
    next;
    mesn;
    mesq l("The Alliance have records of everyone. And if you need another trip, talk to me!");
    tutmes l("PROTIP: Every quest you complete in a location, will make ship travels to and from them cheaper!"), "Protip", false;
    next;
    addtimer 2500, "Inac::OnShout";
    closeclientdialog;
    close;

L_TestServer:
    mesc l("THIS IS MOUBOOTAUR LEGENDS TEST SERVER."), 1;
    mesc l("Progress on this server may be %s.", b(l("lost forever"))), 1;
    mes "";
    mesc l("Unless you know what you are doing, please go to Main Server instead."), 1;
    mesc l("Main server is: %s", b("server.moubootaurlegends.org")), 2;
    mes "";
    mesf "@@help://test-server|%s@@", l("more information about test server ->");
    next;
    goto L_Main;

L_Close:
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
