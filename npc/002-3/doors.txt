// TMW2 Scripts.
// Author:
//    Jesusalva
// Description:
//    Nard's ship Doors NPCs.

002-3,30,28,0	script	AreaNPC#doors4	NPC_HIDDEN,0,0,{

OnTouch:
    if (getq(ShipQuests_Julia) < 3) {
        showavatar NPC_NARD;
        setcamnpc "Nard";
        mesn l("Nard");
        mesq l("Where do you think you are going, without talking to me first?");
        next;
        mesc l("Talk to Nard to unlock the door.");
        next;
        restorecam;
        close;
    }

    if (LOCATION$ == "Candor") {
        warp "005-1", 49, 117;
        close;
    }
    if (LOCATION$ == "Tulim") {
        if ($@MK_SCENE == MK_SIEGE_TULIM && BaseLevel < 30) {
            showavatar NPC_NARD;
            setcamnpc "Nard";
            mesn l("Nard");
            mesc l("The Monster Army is currently sieging Tulimshar. There are hundreds of dangerous monsters out there right now."), 1;
            next;
            mesn l("Nard");
            mesc l("I would advise you to remain here in the ship, they should leave in a few minutes, but I will not force you."), 1;
            next;
            mesn l("Nard");
            mesc l("Just beware that if you leave, you might get killed really quickly. No death penalty, though."), 1;
            next;
            mesc l("Leave the ship? The town have no death penalty and is under a global event."), 1;
            if (askyesno() == ASK_NO)
                close;
            // Boost their evade and HP so they don't get instantly KO'ed (5 min)
            sc_start SC_INCFLEE, 300000, 200;
            sc_start SC_INCMHP, 300000, 1000;
            heal 1000, 0;
            dispbottom l("Queen Of Dragons: It is dangerous to go out alone, I'll raise your evasion and life!"); // QoD = “Demure”.
        }
        warp "003-1", 81, 68;
        close;
    }
    Exception("Script error: 002-3 door");
    close;


}

002-3,42,25,0	script	AreaNPC#002-3d	NPC_HIDDEN,0,0,{

OnTouch:
    if (LOCATION$ == "")
        warp "002-1", 72, 29;
    else
        warp "002-1@"+LOCATION$, 72, 29;
    close;
}
