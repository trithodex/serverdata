// TMW 2 Script
// Author:
//  Jesusalva
//  Micksha
// Description:
//  Controls jails and lockpicks
//  Also creates siege towers to keep prisoners in check :3

042-3,0,0,0	script	#KSlimeSpawn	NPC_HIDDEN,{
    end;

OnKillSlime:
    debugmes "Slime slain";
    if (!playerattached()) {
        debugmes "[ERROR] Player not Attached on Slime Death D:";
        debugmes "[ERROR] Cannot retrieve coordinates!!";
    }
    if (@lockpicks)
        end;
    if (rand2(10000) > 1000) {
        goto OnFirstSlime;
    }
    @lockpicks=true;
    getitem Lockpicks, 1;
    getitem TreasureKey, 1;
    end;

OnFirstSlime:
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        debugmes "[ERROR] [KAMELOT] Unable to spawn for Kamelot %s", .map$;
        debugmes "[ERROR] [KAMELOT] Using dummy data (returned: %d)", .@g;
        .@g=0;
    }

    // let's not trust .name$
    .@label$="#KSlimeSpawn::OnKillSlime";
    if (!playerattached()) {
        debugmes "[ERROR] Player not Attached on Slime Spawn D:";
        debugmes "[ERROR] Cannot retrieve coordinates!!";
    }
    getmapxy(.@m$, .@x, .@y, 0);
    sleep2(1800);
    .@mob=monster(.@m$, .@x, .@y, strmobinfo(1, CopperSlime), CopperSlime, 1, .@label$);
    // This should wipe the monster experience value
    setunitdata(.@mob, UDT_LEVEL, 1);
    //if ($@GM_OVERRIDE) debugmes "Slime is back: %s [%d]", .@label$, .@mob;
    end;


// Spawn Siege Towers
OnInstanceInit:
    .@m$=instance_mapname("042-3");
    debugmes "Kamelot Init: Original %s Target %s", .map$, .@m$; // Fun fact
    monster(.@m$, 89, 133, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 67, 114, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 30, 121, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 23, 137, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 29, 103, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 61, 91, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 75, 69, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 34, 72, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 60, 59, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 87, 32, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 47, 32, strmobinfo(1, SiegeTower), SiegeTower, 1);
    monster(.@m$, 31, 35, strmobinfo(1, SiegeTower), SiegeTower, 1);
    end;
}

// Lockpicks functions
function	script	KamelotLockpick	{
    // Args: x, y, name. Needs player attached
    .@x=getarg(0);
    .@y=getarg(1);
    .@name$=getarg(2);
    mes l("A complex lock seems to be posing a threat to you.");
    next;
    mes l("But thanks to your %s skills, maybe you can pry this open.", thiefrank());
    next;

    .@s=LockPicking(5, 3, false);

    // You broke free!
    if (.@s) {
        .@label$=instance_npcname(.@name$)+"::OnKamelotSlide";
        addtimer 10, .@label$;
        areatimer getmap(), .@x-1, .@y-1, .@x+1, .@y, 10, .@label$;
        return;
    }
    mes l("What's this dark magic, the password has changed!");
    .@label$=instance_npcname("#KSlimeSpawn")+"::OnKillSlime";

    // Give player a easy way to get lockpicks for this
    // (Overrides original .@x/.@y variables)
    if (countitem(Lockpicks) <= 1) {
        @lockpicks=false;
        getmapxy(.@m$, .@x, .@y, 0);
        .@mob=monster(.@m$, .@x, .@y, strmobinfo(1, CopperSlime), CopperSlime, 1, .@label$);
        // This should wipe the monster experience value
        setunitdata(.@mob, UDT_LEVEL, 1);
    }
    return;
}

// Cell Doors
042-3,33,137,0	script	Cell Door#K01	NPC_NO_SPRITE,{
    KamelotLockpick(.x, .y, .name$);
    close;
OnInit:
OnInstanceInit:
    .distance=2;
    end;

OnKamelotSlide:
    .@label$=instance_npcname(.name$)+"::OnKamelotSlide";
    deltimer .@label$;
    dispbottom l("You're finally free!");
    slide .x, .y+1;
    @lockpicks=true;
    end;
}

// Duplication of doors
042-3,84,129,0	duplicate(Cell Door#K01)	Cell Door#K02	NPC_NO_SPRITE
042-3,41,121,0	duplicate(Cell Door#K01)	Cell Door#K03	NPC_NO_SPRITE
042-3,74,109,0	duplicate(Cell Door#K01)	Cell Door#K04	NPC_NO_SPRITE
042-3,36,98,0	duplicate(Cell Door#K01)	Cell Door#K05	NPC_NO_SPRITE
042-3,57,86,0	duplicate(Cell Door#K01)	Cell Door#K06	NPC_NO_SPRITE
042-3,79,65,0	duplicate(Cell Door#K01)	Cell Door#K07	NPC_NO_SPRITE
042-3,43,69,0	duplicate(Cell Door#K01)	Cell Door#K08	NPC_NO_SPRITE
042-3,24,69,0	duplicate(Cell Door#K01)	Cell Door#K09	NPC_NO_SPRITE
042-3,86,24,0	duplicate(Cell Door#K01)	Cell Door#K10	NPC_NO_SPRITE
042-3,59,51,0	duplicate(Cell Door#K01)	Cell Door#K11	NPC_NO_SPRITE
042-3,38,29,0	duplicate(Cell Door#K01)	Cell Door#K12	NPC_NO_SPRITE




// Leave the dungeon
042-3,62,19,0	script	#KDoor0423	NPC_HIDDEN,1,0,{
    end;

OnTouch:
    .@g=getcharid(2);
    warp "042-4@"+.@g, any(59,60), 77;
    addtimer 1000, .name$+"::OnHey";
    end;

OnHey:
    dispbottom l("Oh, here the path seems to split. Which way should we go?");
    addtimer 3000, .name$+"::OnHey2";
    end;

OnHey2:
    dispbottom l("Or should we even split ourselves to check all possible ways?")+" "+col(l("[Caution, this may be dangerous!]"), 1);
    addtimer 7000, .name$+"::OnHey3";
    end;

OnHey3:
    dispbottom l("Also, I don't think we will be able to go back if we pick the wrong way.");
    addtimer 5000, .name$+"::OnHey4";
    end;

OnHey4:
    dispbottom l("If we don't know where to go - Maybe we should go back looking for clues?");
    end;
}

// Required exit. This one has no conditions, so it is not really required
// But I do not trust Instancing System, so better safe than sorry!
042-3,58,140,0	script	#KDoor0423B	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@g=getcharid(2);
    warp "042-2@"+.@g, 41, 23;
    end;
}

