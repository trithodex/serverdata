// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Special Soul Menhir which only allows leaving the map.

001-12,54,16,0	script	Soul Stone#001-12	NPC_SOUL_CLEAN,{
    mesn;
    mes l("(A mystical aura surrounds this stone. It probably can return you home. What do you do?)");

    menu
        l("Touch it."), L_Warp,
        l("Return to main island."), L_WarpEnchanted,
        l("Leave it alone."), -;
    close;

L_Warp:
    warp "Save", 0, 0;
    close;

L_WarpEnchanted:
    warp "001-11", 37, 31;
    close;

OnInit:
    .distance = 10;
    end;
}

