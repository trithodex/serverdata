// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    001-7 Monster King's Village Configuration File
//    Part of Player Quest, see 023-3 scope and 024-16
//    (C) Moubootaur Legends, 2019

001-7,91,89,0	script	#Init0233	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@q=getq(General_Narrator);
    .@q2=getq2(General_Narrator);
    // Cheater Detected
    if (.@q < 15) {
        warp "Save", 0, 0;
        die();
        end;
    }
    if (.@q == 15) {
        dispbottom lg("I'm not a coward! I must press forward!");
        end;
    }
	.@mapn$="023-3";
    warp .@mapn$, 48, 23;
    end;
}

001-7,59,44,0	script	#Init02331	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@n=getq(General_Narrator);
    .@q=getq2(FrostiaQuest_Homunculus);
    // Cheater Detected
    if (.@n < 15) {
        warp "Save", 0, 0;
        die();
        end;
    }
    mesn l("Magically Sealed Gate");
    mesc l("The door is sealed. The riddle says: “I drink, I become, I am. Don't say my name, but say why you know me. For, I am the best in the world.”");
    if (!(.@q & 1)) {
        mesc l("You have no idea what that means."), 1;
        close;
    } else {
        mesc l("You know the answer can only be @@.", getitemlink(Coffee)), 3;
    }
    next;
    mesn l("Magically Sealed Gate");
    if (!(.@q & 256)) {
        mesc l("Even after breaking the first layer, a second layer keeps active. The first layer gets back to work shortly after. What have I missed or forgotten to do?"), 1;
        close;
    } else {
        if (.@q != 511)
            Exception("Invalid quest state: "+.@q, RB_DEFAULT|RB_SPEECH|RB_ISFATAL);
        mesc l("Are you sure you want to proceed? You CANNOT COME BACK!"), 1;
        if (askyesno() == ASK_NO)
            close;
    }
    closeclientdialog;
    // We can create instance without recording the ID etc.
	// Map name limit: 4 chars (hmc1) - as of homunculus
	.@mapn$="hmc1@"+getcharid(0);
	.@map2$="hmc2@"+getcharid(0);
	.@inst = instance_create("Homunculus "+getcharid(0), getcharid(3), IOT_CHAR);
    if (.@inst >= 0) {
        instance_attachmap("023-3-1", .@inst, false, .@mapn$);
        instance_attachmap("023-3-2", .@inst, false, .@map2$);
        // Instance lasts one hour
	    instance_set_timeout(3600, 3600, .@inst);
	    instance_init(.@inst);
    }
    setq1 FrostiaQuest_Homunculus, 2;
    warp .@mapn$, any(45,46), 79;
    @instid=.@inst;
    end;
}

001-7,50,112,0	script	Abandoned Fountain#MKH	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("At a first glance, it seems to be full of water, but inspecting closer, it is not.");
    next;
    mesn;
    mesc l("It is difficult to describe, it is like if it was mixed with mana itself. Drinking a bit of it was enough to recover your MP.");
    percentheal 0, 100;
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=1;
    .distance=2;
    end;
}


001-7,48,111,0	script	Sign#MKH	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("Welcome to") + " --_--_-___--__-_-_.";
    mesc l("Yes, we have @@!", getitemlink(Coffee));
    next;
    mesn;
    mesc l("The village name is difficult to read.");
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=2;
    .distance=2;
    end;
}

001-7,41,104,0	script	Abandoned House#MKH1	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("The door won't budge.");
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=4;
    .distance=2;
    end;
}

001-7,59,104,0	script	Abandoned House#MKH2	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("It seems to have been abandoned a long time ago, but the chimney is still going?");
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=8;
    .distance=2;
    end;
}


001-7,41,99,0	script	Abandoned House#MKH3	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("The knob has... melted down? What?");
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=16;
    .distance=1;
    end;
}

001-7,59,99,0	script	Abandoned House#MKH4	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("There seems to be signs of a fight long forgotten, but it still reeks blood.");
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=32;
    .distance=1;
    end;
}

001-7,33,89,0	script	Apple Trees#MKH1	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    mesn;
    mesc l("It seems to be growing apples, but by the amount of magic particles...");
    next;
    mesn;
    mesc l("A close inspection reveals nothing out of ordinary. It seems to be well kept.");
    next;
    if (!(.@q & 1)) {
        mesn strcharinfo(0);
        mesc l("You're hesitant to pick one, they could be dangerous.");
        close;
    }
    mesn strcharinfo(0);
    mesc l("You carefully pick a @@. It looks delicious! You feel you'll need it sooner than you expect.", getitemlink(MagicApple));
    if (!(.@q & .hcID)) {
        inventoryplace MagicApple, 1;
        getitem MagicApple, 1;
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    }
    close;
OnInit:
    .hcID=64;
    .distance=2;
    end;
}

001-7,40,88,0	script	Abandoned House#MKH6	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    if (!(.@q & 64)) {
        mesn strcharinfo(0);
        mesc l("I should check the Apple Garden first.");
        close;
    }
    // I hope this is right
    if (!(
        (.@q & 4) &&
        (.@q & 8) &&
        (.@q & 16) &&
        (.@q & 32)) ) {
        mesn strcharinfo(0);
        mesc l("I better not disturb the hut owner.");
        close;
    }

    mesn;
    mesc l("It's locked. But a close inspection reveals a small key under the rug.");
    next;
    mesn;
    mesc l("Maybe there's a locked door somewhere, and this key will fit?");
    if (!(.@q & .hcID))
        setq2 FrostiaQuest_Homunculus, .@q|.hcID;
    close;
OnInit:
    .hcID=128;
    .distance=1;
    end;
}

001-7,50,99,0	script	Abandoned House#MKH5	NPC_NO_SPRITE,{
    .@q=getq2(FrostiaQuest_Homunculus);
    if (!(.@q & 128)) {
        mesn;
        mesc l("It's locked.");
        next;
        mesn strcharinfo(0);
        mesc l("Maybe there is a key somewhere near. I should keep looking.");
        close;
    }
    mesn;
    mesc l("It's locked.");
    next;
    mesn;
    mesc l("You use the small key from the apple garden hut. It fits neatly.");
    next;
    mesn;
    mesc l("The hut is... empty. And it doesn't have a fireplace, either.");
    next;
    mesn;
    mesc l("You don't know how the chimney keeps producing smoke. It must be using hiding magic.");
    next;
    mesn;
    mesc l("Whoever took control of this village is no ordinary mage. There's a switch on the wall.");
    next;
    mesn;
    mesc l("You flip the switch. Nothing happens.");
    // You can unflip it :>
    setq2 FrostiaQuest_Homunculus, .@q^.hcID;
    mesc l("Strange switch status: @@", (.@q&.hcID ? l("Inactive") : l("Active"))), 3;
    close;
OnInit:
    .hcID=256;
    .distance=1;
    end;
}

