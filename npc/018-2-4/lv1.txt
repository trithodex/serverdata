// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Heroes Hold Exchanger - Level 1

018-2-4,33,28,0	script	Novice Exchanger#0	NPC_M_COINKEEPER,{
    .@q=getq2(LoFQuest_HH);
    if (.@q & HH_NOVICE)
        openshop;
    goodbye;
    closedialog;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, TopHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CandorShirt);
    setunitdata(.@npcId, UDT_WEAPON, CandorShorts);
    setunitdata(.@npcId, UDT_HEADBOTTOM, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 25);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex = G_OTHER;
    .distance = 5;
    npcsit;

	tradertype(NST_CUSTOM);

	sellitem EquipmentBlueprintA,1600;
	sellitem RaidTrousers,1200;
	sellitem BugSlayer,800;
	sellitem LeatherShield,600;
	sellitem TolchiAmmoBox,270;
	sellitem Grenade,96;

	sellitem HastePotion,50;
	sellitem StrengthPotion,50;

	sellitem Curshroom,10;
	sellitem SmokeGrenade,4;
	end;

OnCountFunds:
	setcurrency(countitem(HeroCoin));
	end;

OnPayFunds:
	if( countitem(HeroCoin) < @price )
		end;
	delitem HeroCoin,@price;
	purchaseok();
	end;

}

018-2-4,33,49,0	duplicate(Novice Exchanger#0)	Novice Exchanger#1	NPC_M_COINKEEPER

