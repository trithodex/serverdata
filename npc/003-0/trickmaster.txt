// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Trickmaster of Tricksters Class

003-0,35,42,0	script	Trickmaster	NPC_SITTED_NINJA,{
    function basicMagic;
    function standardMagic;
    function advancedMagic;
    mes l(".:: Trickster Class ::.");
    mesc l("Specialized in miscellaneous skills.");
    next;
    mesn;
    mesc l("You have @@ magic skill points available.", sk_points());
    select
        l("Basic Tricks"),
        l("Standard Tricks"),
        l("Advanced Tricks");
    mes "";
    .@lv=@menu;
    do
    {
        // Display appropriate menu
        if (.@lv == 1)
            basicMagic();
        else if (.@lv == 2)
            standardMagic();
        else if (.@lv == 3)
            advancedMagic();

        // Handle result
        mes "";
        if (@menuret) {
            if (!learn_magic(@menuret)) {
                mesc l("You do not meet all requisites for this skill."), 1;
                next;
            }
        } else {
            closeclientdialog;
        }

    } while (@menuret);
    close;

L_NoMagic:
    next;
    mesn;
    mesq l("You do not have enough magic power for these classes.");
    next;
    mesn;
    mesq l("Besides the Magic Council, Andrei Sakar have his own Mana Stone, but I doubt he would train the likes of you, or share his Mana Stone.");
    next;
    mesn;
    mesq l("Perhaps, in the city, someone knows rumors about Mana Stones and can teach you. Other than that, you're on your own.");
    close;

function basicMagic {
    mes l(".:: Mana Bomb ::.");
    mesc l("Converts all your mana in damage. Damages all enemies in same tile.");
    mes "";
    mes l(".:: Backsliding ::.");
    mesc l("Instantly jumps 5 tiles backwards.");
    mes "";
    mes l(".:: Nature Wall ::.");
    mesc l("Create a natural wall under the cursor, to delay your enemies.");
    mes "";
    mes l(".:: Archers Eye ::.");
    mesc l("Increase bow range and accuracy.");
    mes "";
    mes l(".:: First Aid ::.");
    mesc l("Recover some HP.");
    mes "";
    menuint
        l("Mana Bomb"), TMW2_MANABOMB,
        l("Backsliding"), TF_BACKSLIDING,
        l("Nature Wall"), MG_FIREWALL,
        l("Archers Eye"), AC_VULTURE,
        l("First Aid"), TMW2_FIRSTAID,
        l("Cancel"), 0;
    return;
}

function standardMagic {
    if (!MAGIC_LVL) goto L_NoMagic;
    mes l(".:: Free Cast ::.");
    mesc l("Allows to attack right after casting.");
    mes "";
    mes l(".:: Full Throttle ::.");
    mesc l("An emergency skill which temporarily raises all your stats.");
    mes "";
    mes l(".:: Sudden Attack ::.");
    mesc l("Instantly jumps to target and delivers an attack.");
    mes "";
    mes l(".:: Trick Dead ::.");
    mesc l("Plop dead in the ground. Enemies won't attack you this way.");
    mes "";
    menuint
        l("Free Cast"), SA_FREECAST,
        l("Full Throttle"), ALL_FULL_THROTTLE,
        l("Sudden Attack"), GC_DARKILLUSION,
        l("Trick Dead"), NV_TRICKDEAD,
        l("Cancel"), 0;
    return;
}

function advancedMagic {
    if (MAGIC_LVL < 2) goto L_NoMagic;
    mes l(".:: There are no skills ::.");
    mesc l("You can bug Jesusalva to extend the Battlefield Control skills.");
    mes "";
    menuint
        l("Cancel"), 0;
    return;
}

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;

}

