// TMW2 Script
// Author:
//    Jesusalva

006-0,41,36,0	script	Sign#0060PI	NPC_SWORDS_SIGN,{
    mesc l("To the lovely cuteness; Unwavering helpfulness.");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

// Effective warp
006-0,40,35,0	script	Magic Barrier#Pi	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    warp "006-2", 68, 117;
    end;
}
