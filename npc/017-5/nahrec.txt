// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    General Purpose Blacksmith
//          TODO: Mylarin Dust for THE EPISODE. This means Savior Pants is part of THE EPISODE
//          TODO: Silversmith on 24, 25

017-5,44,24,0	script	Nahrec	NPC_PLAYER,{
    function blacksmith_create;
    function NahrecStory;
    function NahrecMain;
    function NahrecEnd;
    function NahrecSavior;
    function Mylarin;

    mesn;
    mesq l("Almost there! The War Lord will be proud with this new armor I am inventing for him! Just a little more...!");
    next;

    mesn;
    mesq l("Ah, hello! It is good to see another traveler in this town!");
    next;
    select
        l("Really? Where are you from?"),
        l("It's good to see you too, do you happen to be able to make stuff?"),
        l("Alright.");
    mes "";
    if (@menu == 1)
        NahrecStory();
    if (@menu == 2) // Changed by option 1, do not use a switch
        NahrecMain();
    NahrecEnd(); // changed by option 2, do not use a switch

function NahrecEnd {
    closedialog;
    goodbye;
    close;
}

function NahrecStory {
    mesn;
    mesq l("I came from Thermin, a town far far away.");
    next;
    mesn;
    mesq l("I'm an experienced weapon master helping this smithy here, but I am new in this town. I am best at smithing armor.");
    next;
    select
        l("Good to know."),
        l("'Best'? So you can make other things?");
    return;
}

function NahrecMain {
  do
  {
    mesn;
    mesq l("Yes, I can craft many things. In addition to plating and improving armor, I can craft smaller items made of gold and other metals.");
    select
        rif(!RECIPES_EQUIPMENT[CraftGoldenLightPlatemail], l("Can you improve my Light Platemail for me?")),
        rif(!RECIPES_EQUIPMENT[CraftGoldenWarlordPlate], l("Can you improve my Warlord Plate for me?")),
        l("Can you craft Chainmail?"),
        l("Can you craft Light Platemail?"),
        l("Can you craft Warlord Plate?"),
        l("Can you craft Savior Pants?"),
        l("Can you craft Chainmail Skirt?"),
        l("Do you know something about 'Mylarin Dust'?"),
        l("Nevermind, bye!");
    mes "";
    switch (@menu) {
        case 1:
            mesn;
            mesq l("For %d GP I'll teach you, do we have a deal?", fnum(30000));
            next;
            if (askyesno() == ASK_YES && Zeny > 30000) {
                mes "";
                Zeny-=30000;
                RECIPES_EQUIPMENT[CraftGoldenLightPlatemail]=true;
                mesn;
                mesc l("Nahrec discretly gives you a recipe.");
                mesc l("\"*psst, just follow what is outlined here to make it golden.*\"");
            }
            break;
        case 2:
            mesn;
            mesq l("For %d GP I'll teach you, do we have a deal?", fnum(200000));
            next;
            if (askyesno() == ASK_YES && Zeny > 200000) {
                mes "";
                Zeny-=200000;
                RECIPES_EQUIPMENT[CraftGoldenWarlordPlate]=true;
                mesn;
                mesc l("Nahrec discretly gives you a recipe.");
                mesc l("\"*psst, just follow what is outlined here to make it golden.*\"");
            }
            break;
            break;
        case 3:
            blacksmith_create(IronIngot, 10, SilverIngot, 2, Chainmail, 20000);
            break;
        case 4:
            blacksmith_create(IronIngot, 15, SilverIngot, 5, LightPlatemail, 40000);
            break;
        case 5:
            blacksmith_create(IronIngot, 5, GoldenLightPlatemail, 1, WarlordPlate, 5000);
            break;
        case 6:
            NahrecSavior();
        case 7:
            blacksmith_create(IronIngot, 10, SilverIngot, 15, ChainmailSkirt, 35000);
            break;
        case 8:
            Mylarin();
        case 9:
            return;
    }
  } while (true);
  return;
}

function Mylarin {
    mesn;
    mesq l("Mylarin dust?! Where would you get that? That's amazing!");
    next;
    mesq l("It has eluded so many craftsmiths, who knows what can be done with that!");
    next;
    mesq l("I can make some stuff with it, but I have no idea where it can be found. Mylarin dust... amazing.");
    next;
    return;
}

function NahrecSavior {
    mesn;
    mesq l("Yes, I can craft Savior Pants. But it is not cheap. I'll need Platinum, Iridium, and Mylarin Dust. And gold. Much gold.");
    select
        l("I'll be back with those items.."),
        l("That's crazy!");
    mes "";
    return;
}

// blacksmith_create( BaseItem1, Amount, BaseItem2, Amount, PrizeItem, Price )
function blacksmith_create {
    .@base1=getarg(0);
    .@amon1=getarg(1);
    .@base2=getarg(2);
    .@amon2=getarg(3);
    .@prize=getarg(4);
    .@price=getarg(5);

	.@price=POL_AdjustPrice(.@price);

    mesn;
    mesq l("Do you want to craft @@? For that I will need @@ @@, @@ @@ and @@ gp.",
    getitemlink(.@prize), .@amon1, getitemlink(.@base1), .@amon2, getitemlink(.@base2), .@price);

    select
        l("Yes"),
        l("No");

    if (@menu == 2)
        return;

    if (countitem(.@base1) >= .@amon1 &&
        countitem(.@base2) >= .@amon2 &&
        Zeny >= .@price) {
        inventoryplace .@prize, 1;
        delitem .@base1, .@amon1;
        delitem .@base2, .@amon2;
		POL_PlayerMoney(.@price);
        if (.@prize == WarlordPlate) {
            .@b=any(IOPT_SPLASHDAMAGE, VAR_ITEMDEFPOWER);
            if (.@b == IOPT_SPLASHDAMAGE)
                CsysNpcCraft(.@prize, .@b, 1);
            else
                CsysNpcCraft(.@prize, VAR_ITEMDEFPOWER, rand2(50), VAR_MDEFPOWER, 30);
        } else {
            CsysNpcCraft(.@prize, IOPT_SCRESIST_POISON, 10, IOPT_SCRESIST_SILENCE, 10, IOPT_SCRESIST_BLIND, 10, IOPT_SCRESIST_CURSE, 10);
        }

        mes "";
        mesn;
        mesq l("Many thanks! Come back soon.");
    } else {
        speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                l("You don't have enough material, sorry.");
    }
    return;
}

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, WarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, SaviorPants);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;

}
