// TMW2 Scripts
// Author:
//    Jesusalva, Saulc
// Description:
//      Slot Machine for bets in Hurnscald
//      Rare Reward: Paper Bag

012-7,53,31,0	script	Slot Machine#012-7a	NPC_SLOTMACHINE,{
    function symbol {
        switch (getarg(0)) {
        case 1:
            mesn "%%A";
            break;
        case 2:
            mesn "%%B";
            break;
        case 3:
            mesn "%%C";
            break;
        case 4:
            mesn "%%D";
            break;
        case 5:
            mesn "%%E";
            break;
        case 6:
            mesn "%%F";
            break;
        case 7:
            mesn "7";
            break;
        case 8:
            mesn "%%8";
            break;
        default:
            mesn "%%@";
            break;
        }
        return;
    }

L_Menu:
    mesn;
    mesc l("Spin three symbols, and jackpot great rewards!");
    mesc l("Just one coin for spin.");
    next;
    menu
        rif(countitem(CasinoCoins) >= 1, l("Spin!")), L_Spin,
        l("Prizes"), L_Info,
        l("Leave"), -;
    close;

L_Info:
    mes "";
    mesc l("Prizes:");
    mes l("##9 777: @@.", getitemlink(PaperBag));
    mesc l("Three equal: @@.", "20 casino coins");
    mesc l("Two equal: 1 casino coin.");
    next;
    goto L_Menu;


L_Spin:
    mesc l("Spinning...");
    next;
    delitem CasinoCoins, 1;
    .@a=rand2(1,8);
    .@b=rand2(1,8);
    .@c=rand2(1,8);
    symbol(.@a);
    symbol(.@b);
    symbol(.@c);
    next;
    mesn;
    if (.@a == .@b && .@a == .@c && .@a == 7) {
        getitem PaperBag, 1;
        mesc l("Jackpot! You got the Paper Bag!"), 3;
    } else if (.@a == .@b && .@a == .@c) {
        getitem CasinoCoins, 20;
        mesc l("Congrats! A pity it was not 777..."), 3;
    } else if (.@a == .@b || .@a == .@c || .@b == .@c) {
        getitem CasinoCoins, 1;
        mesc l("Lucky! You got the coin back!"), 3;
    } else {
        mesc l("It wasn't this time..."), 3;
    }
    next;
    goto L_Menu;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
	end;


}

