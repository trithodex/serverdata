// TMW2 Script
// Author:
//    TMW Org.
//    Jesusalva
// Description:
//    Part of Anwar Field quest

004-1,107,67,0	script	Anwar	NPC_RAIJIN,{
    .@q=getq(TulimsharQuest_AnwarField);
    if (BaseLevel < 18) goto L_Weak;

    if (.@q > 10) goto L_Complete;
    if (.@q == 10) goto L_SecondReward;
    if (.@q == 9) goto L_FirstReward;
    if (.@q == 8) goto L_SecondTry;
    if (.@q == 7) goto L_AnwarField;
    if (.@q == 6) goto L_FirstTry;
    if (.@q == 5) goto L_TryIt;
    if (.@q >= 1) goto L_FirstTry;

    mesn;
    mesq l("Hi. Could you perhaps be interested in doing some small errand for me?");
    menu
        l("Sure!"),L_Sure,
        l("I'm busy, sorry."),L_Close;

L_Complete:
    mesn;
    mesq l("Thanks for saving Tulimshar from a famine. I'll be forever grateful.");
    next;
    mesn;
    mesq l("Dealing with elves is too bothersome to me.");
    close;

L_AnwarField:
    mesn;
    mesq l("My crops! Hurry up, and talk to Eomie!!");
    close;

L_Sure:
    mes "";
    mesn;
    mesq l("Great! Eomie, the girl on Tulimshar's magic academy, is an alchemist. She probably makes fertilizers.");
    next;
    mesn;
    mesq l("This farm is dying due constant monster attacks, and without them, Tulimshar might face a famine.");
    next;
    mesn;
    mesq l("Please, talk to her. Maybe she understands the direness of the situation and help... But you know. Elves.");
    setq TulimsharQuest_AnwarField, 1;
    close;

L_FirstTry:
    mesn;
    mesq l("Good luck getting the fertilizer from Eomie! Many elves simply refuse to cooperate until it affects them directly.");
    close;

L_TryIt:
    .@q2=getq2(TulimsharQuest_AnwarField);
    mesn;
    mesq l("You've brought me fertilizer! Let me see if it works...");
    next;
    setq2 TulimsharQuest_AnwarField, .@q2+1;

    // Fail chances are 100% - 13% per attempt
    if (rand2(0,100) < 100-(.@q2*13)) {
        setq1 TulimsharQuest_AnwarField, 6;
        mesc l("Nothing happens.");
        next;
        mesn;
        mesq l("Uh... Something should happen, right? Can you get another one?");
    } else {
        setq1 TulimsharQuest_AnwarField, 7;
        mesc l("Evil worms crawl from earth and starts devouring the plants!");
        next;
        mesn;
        mesq l("Uh... That should not happen, right? RIGHT?");
        next;
        mesn;
        mesq l("Don't just stand here! Go fetch help, NOW!!");
    }

    close;

L_SecondTry:
    mesn strcharinfo(0);
    mesq l("Here is the bug bomb! Eomie just gave me. Hurry up!");
    next;
    getexp 20, 0;
    specialeffect(51);
    setq TulimsharQuest_AnwarField, 9;
    mesn;
    mesq l("Thanks God... The crops are safe. Not only that, but the fertilizer works!");
    next;
    mesn;
    mesq l("Ah, that was tiresome... I'll go make a reward for them, talk to me again later.");
    close;

L_FirstReward:
    mesn;
    mesq l("Here are two @@. Please deliver it to them. I hope they'll like it.", getitemlink(TortugaShell));
    setq TulimsharQuest_AnwarField, 10, 0;
    getitembound(TortugaShell, 2, 4); // Prevent accidental item loss
    close;

L_SecondReward:
    .@q2=getq2(TulimsharQuest_AnwarField);
    if (.@q2 < 3){
        mesn;
        mesq l("Please deliver the two @@ to Tinris and Eomie, and then I'll give you something for your help.", getitemlink(TortugaShell));
        close;
    }
    setq TulimsharQuest_AnwarField, 11, 0;
    getitem2(FarmerPants, 1, 1, 0, 0, OrangeDye, 0,0,0); // EXPERIMENTAL, required for Inspector Quest
    getexp 750, 0;
    mesn;
    mesq l("Many thanks for your help! Here, take this. I'm sure it can be very useful later. It always is.");
    close;

L_Weak:
    hello;
    end;

L_Close:
    close;

OnTimer1000:
    domovestep;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FarmerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, DesertShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, LeatherTrousers);
    setunitdata(.@npcId, UDT_WEAPON, Boots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 6);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 14);

    .sex = G_MALE;
    .distance = 5;

    initpath "move", 93, 84,//A
             "dir", RIGHT, 0,
             "wait", 30, 0,
             "move", 84, 60,//C
             "dir", UP, 0,
             "wait", 12, 0,
             "move", 117, 64,//K
             "dir", LEFT, 0,
             "wait", 8, 0,
             "move", 95, 84,//B
             "dir", DOWN, 0,
             "wait", 29, 0,
             "move", 92, 68,//R
             "dir", DOWN, 0,
             "wait", 14, 0,
             "move", 94, 80,//F
             "dir", UP, 0,
             "wait", 13, 0,
             "move", 130, 73,//I
             "dir", DOWN, 0,
             "wait", 14, 0,
             "move", 95, 84,//B
             "dir", DOWN, 0,
             "wait", 28, 0,
             "move", 93, 64,//D
             "dir", UP, 0,
             "wait", 11, 0,
             "move", 108, 60,//M
             "dir", RIGHT, 0,
             "wait", 8, 0,
             "move", 93, 84,//A
             "dir", RIGHT, 0,
             "wait", 27, 0,
             "move", 98, 60,//O
             "dir", UP, 0,
             "wait", 18, 0,
             "move", 89, 62,//P
             "dir", DOWN, 0,
             "wait", 7, 0,
             "move", 95, 84,//B
             "dir", DOWN, 0,
             "wait", 26, 0,
             "move", 85, 66,//Q
             "dir", UP, 0,
             "wait", 21, 0,
             "move", 89, 82,//G
             "dir", UP, 0,
             "wait", 10, 0,
             "move", 93, 84,//A
             "dir", RIGHT, 0,
             "wait", 25, 0,
             "move", 105, 78,//H
             "dir", UP, 0,
             "wait", 17, 0,
             "move", 114, 63,//L
             "dir", UP, 0,
             "wait", 1, 0,
             "move", 95, 84,//B
             "dir", DOWN, 0,
             "wait", 24, 0,
            "move", 109, 69,//J
             "dir", DOWN, 0,
             "wait", 15, 0,
             "move", 104, 63,//N
             "dir", LEFT, 0,
             "wait", 1, 0,
             "move", 95, 84,//B
             "dir", DOWN, 0,
             "wait", 23, 0,
             "move", 94, 80,//F
             "dir", UP, 0,
             "wait", 9, 0,
             "move", 75, 63,//S
             "dir", UP, 0,
             "wait", 15, 0;

    initialmove;
    initnpctimer;
    end;
}

