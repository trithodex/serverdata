// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Several filters

// filter_always( id )
function	script	filter_always	{
    return true;
}

// filter_onlyme( id )
function	script	filter_onlyme	{
    return (getarg(0) == getcharid(3));
}

// filter_notme( id )
function	script	filter_notme	{
    return (getarg(0) != getcharid(3));
}

// filter_sameguild( id )
function	script	filter_sameguild	{
    if (getcharid(2) < 1)
        return false;
    return (strcharinfo(2, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(2));
}

// filter_sameguildnotyou( id )
function	script	filter_sameguildnotyou	{
    if (getcharid(2) < 1)
        return false;
    if (getarg(0) == getcharid(3))
        return false;
    return (strcharinfo(2, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(2));
}

// filter_sameparty( id )
function	script	filter_sameparty	{
    if (getcharid(1) < 1 && getarg(0) != getcharid(3))
        return false;
    return (strcharinfo(1, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(1));
}

// filter_sameguildorparty( id )
function	script	filter_sameguildorparty	{
    if (getcharid(2) < 1 && getcharid(1) < 1)
        return false;
    .@party=(strcharinfo(1, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(1));
    .@guild=(strcharinfo(2, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(2));
    return ((getcharid(1) > 0 && .@party) || (getcharid(2) > 0 && .@guild));
}

// filter_sameguildorpartynotyou( id )
function	script	filter_sameguildorpartynotyou	{
    if (getarg(0) == getcharid(3))
        return false;
    if (getcharid(2) < 1 && getcharid(1) < 1)
        return false;
    .@party=(strcharinfo(1, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(1));
    .@guild=(strcharinfo(2, "~!<mk>@tmw2.org", getarg(0)) == strcharinfo(2));
    return ((getcharid(1) > 0 && .@party) || (getcharid(2) > 0 && .@guild));
}

// filter_hostile( id )
function	script	filter_hostile	{
    //.@type=getunitdata(getarg(0), UDT_TYPE);
    .@type=getunittype(getarg(0));
    .@chkid=getarg(0);

    // Players outside PVP
    if (.@type == UNITTYPE_PC) {
        getmapxy(.@m$, .@x, .@y, .@type, .@chkid);
        if (!ispvpmap(.@m$))
            return false;
        // FIXME: We already have !(filter_sameguildorparty())
        // We might be over-processing this
        // Honor party flag
        if (!getmapflag(.@mapa$, mf_pvp_noparty) &&
             getcharid(1) == getcharid(1, strcharinfo(0, "", .@chkid)))
            return false;
        // Honor guild flag
        if (!getmapflag(.@mapa$, mf_pvp_noguild) &&
             getcharid(2) == getcharid(2, strcharinfo(0, "", .@chkid)))
            return false;
    }

    // Monsters
    if (.@type == UNITTYPE_MOB)
        return true;

    // NPCs
    if (.@type == UNITTYPE_NPC)
        return false;

    // Homunculus
    if (.@type == UNITTYPE_HOM)
        .@chkid=charid2rid(getunitdata(getarg(0), UDT_MASTERCID));

    // Pets
    if (.@type == UNITTYPE_PET)
        .@chkid=getunitdata(getarg(0), UDT_MASTERAID);

    // Mercenaries
    if (.@type == UNITTYPE_MER)
        .@chkid=charid2rid(getunitdata(getarg(0), UDT_MASTERCID));

    // Elementals
    if (.@type == UNITTYPE_ELEM)
        .@chkid=charid2rid(getunitdata(getarg(0), UDT_MASTERCID));

    //debugmes "filter_hostile: Filtering %d (original %d) (BL %d)", .@chkid, getarg(0), .@type;
    // Players (and slaves)
    return !(filter_sameguildorparty(.@chkid));
}

// filter_friendly( id )
function	script	filter_friendly	{
    return !(filter_hostile(getarg(0)));
}

// filter_notboss( id )
function	script	filter_notboss	{
    return (!(getunitdata(getarg(0), UDT_MODE) & MD_BOSS));
}

