// TMW-2 script.
// Authors:
//    Jesusalva
// Description:
//    Server news.

function	script	GameNews	{
    .@entry=getarg(0, 0);
    switch(.@entry) {
    case 99:
        mes "";
        mes ".:: "+ l("Prologue") + " ::.";
        next;
        mesc l("[@@https://wiki.moubootaurlegends.org/Storyline|Read the History@@]");
        next;
        break;
    //////////////////////////////////////////////////////////////////
    case 100:
        mes "";
        mes ".:: "+ l("The First Act") + " ::.";
        next;
        mesc l("The Monster King Army is occupying several towns! Brave players need to group and retake them!");
        next;
        break;
    //////////////////////////////////////////////////////////////////
    case 101:
        mes "";
        mes ".:: "+ l("The Second Act") + " ::.";
        next;
        mesc l("The Monster King Army left the cities! What will happen next? Anxiety grows!");
        next;
        break;
    //////////////////////////////////////////////////////////////////
    case 102:
        mes "";
        mes ".:: "+ l("The Third Act") + " ::.";
        next;
        mesc l("The Monster King Army is making siege at towns randomly!");
        mesc l("The Army seems to walk away after some time.");
        mesc l("We must defeat the commanders to avoid major damage to them!");
        next;
        break;
    //////////////////////////////////////////////////////////////////
    case 103:
        mes "";
        mes ".:: "+ l("The Fourth Act") + " ::.";
        next;
        mesc l("The Monster Army is in complete disarray, sieges are much less frequent.");
        mesc l("The mist over the Impregnable Fortress Peak finally lowered down, and it seems... The peaks are no more!");
        mesc l("The Monster King Lair is not in a impregnable mountain, but in a small island now!!");
        mesc l("Will adventurers reach it? Will the random attacks at towns cease??");
        next;
        break;
    //////////////////////////////////////////////////////////////////
    case 104:
        mes "";
        mes ".:: "+ l("The Fifth Act") + " ::.";
        next;
        mesc l("We must defeat the Monster King on his evil lair!");
        mesc l("Only then we may have peace!!");
        next;
        break;
    default:
        break;
    }

    mesc l("We want to thank everyone who did this release possible.");
    mes "";
    mesc l("Shall you have any inquiry, do not hesit to [@@mailto:admin@tmw2.org|send us an email@@]##b.");
    mesc l("You can also read the [@@news|server news@@], or even [@@https://tmw2.org/news|older entries@@].");
    next;
    return;
}

function	script	EventHelp	{
    if ($EVENT$ == "") return;

    if ($EVENT$ == "Kamelot") {
        /////////////////////////////////////////////////////////////////////////
        mesc ".:: " + l("Kamelot Raid") + " ::.", 2;
        mes "";
        mes l("Group together your guild and challenge the evil power,");
        mes l("which creeps over Kamelot! During this event, the ancient");
        mes l("evil will return every day to curse King Arthur the Micksha.");
        mes "";
        mes l("Is your guild strong enough to give Arthur at least good nights sleep?");
        mesc l("Location: Kamelot Castle, west of Hurnscald."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Valentine") {
        mesc ".:: " + l("Valentine Day") + " ::.", 2;
        mes "";
        mes l("It is time to send %s to your beloved ones!", getitemlink(BoxOfChocolates));
        mes l("Touch Soul Menhir and visit the Valentine's Island.");
        mes l("Collect chocolate and love letters from the fluffies");
        mes l("and have Demure to send them to those you admire!");
        mes l("Don't forget to collect and eat any chocolate sent to you, too!");
        mes "";
        mes l("Witness, the power of love!");
        mesc l("Location: Valentine Island, access by Soul Menhir."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Easter") {
        mesc ".:: " + l("Easter") + " ::.", 2;
        mes "";
        mes l("The Enchanted Forest is now open to visitors!");
        mes l("Collect easter eggs, and exchange them with Lilica!");
        mes "";
        mes l("Compete for the first place, and remember to exchange silver");
        mes l("easter eggs for more useful things!");
        mes "";
        mes l("Who will collect the most?!");
        mesc l("Location: Enchanted Forest, access by Soul Menhir."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Patrick") {
        mesc ".:: " + l("St. Patrick Day") + " ::.", 2;
        mes "";
        mes l("A golden pot in woodlands shall reward those who wear green.");
        mes l("Find it, spin it, and be bestowed in golden rewards!");
        mes "";
        mes l("Meanwhile, at 00h, 06h, 12h, 15h, 18h and 21h UTC,");
        mes l("legendary clovers will spawn almost everywhere.");
        mes "";
        mes l("Feeling lucky?");
        mesc l("Location: North Woodlands, south of Nivalis."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Worker") {
        mesc ".:: " + l("Worker Day") + " ::.", 2;
        mes "";
        mes l("The International Worker Day is a traditional celebration,");
        mes l("focused on lower level players, and to cherish those whom work hard everyday.");
        mes "";
        mes l("Visit the special event map, kill low level bosses, collect %s,", getitemlink(Pearl));
        mes l("exchange them, and have fun!");
        mes "";
        mes l("Happy %s!", "@@https://en.wikipedia.org/wiki/International_Workers%27_Day|"+l("international worker day")+"@@");
        mesc l("Location: Worker's Cave, access by Soul Menhir."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Thanksgiving") {
        mesc ".:: " + l("Thanksgiving") + " ::.", 2;
        mes "";
        mes l("It has been a great year, and the TMW2 Team would like to");
        mes l("give everyone gifts for spending so much time with us %%l");
        mes "";
        mes l("Every day you login, you'll be able to spin a card.");
        mes l("the card will determine your gift - the more cards you spin,");
        mes l("the better your chances to get the best cards.");
        mes "";
        mes l("So long, and thanks for all the fish!");
        mesc l("Location: Daily Login."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Christmas") {
        mesc ".:: " + l("Christmas") + " ::.", 2;
        mes "";
        mes l("By far, the most important holiday on Moubootaur Legends.");
        mes l("Visit the Christmas Workshop, and talk to the chief in charge.");
        mes "";
        mes l("Seems like they're having difficulty handling the demand, and");
        mes l("need help to send gifts to everyone! Compete for scoreboards,");
        mes l("but remember: Rewards will also be based on everyone's progress!");
        mes "";
        mes l("Merry Christmas, and a happy new year! \\o/");
        mesc l("Location: Christmas Workshop, Romantic Field, south of Nivalis."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Anniversary") {
        mesc ".:: " + l("Moubootaur Legends Anniversary") + " ::.", 2;
        mes "";
        mes l("Moubootaur Legends just got older! %%N");
        mes l("Base Experience Rate is now %d%%.", $BCONFB_EXPR);
        mes "";
        mes l("Also known as TMW2 Day, it celebrates the server founding,");
        mes l("in March 2nd 2018.");
        mes l("New chars, and reborn chars, will also begin at level 10.");
        mes "";
        mes l("Invite your friends, and become a Moubootaur Legend!");
        mesc l("Location: N/A"), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Regnum") {
        mesc ".:: " + l("Regnum Blessing") + " ::.", 2;
        mes "";
        mes l("The Regnum Blessing causes all monsters in an area");
        mes l("to give %s experience!", l("triple"));
        mes "";
        mes l("Farm there and get the best experience value!");
        mesc l("Location: %s", $REGNUM_BLESSMAP_H$), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Expo") {
        mesc ".:: " + l("World Expo") + " ::.", 2;
        mes "";
        mes l("A few times during the year, an independent team organizes a world exposition of special relics.");
        mes l("However, %s stole the treasures which were being showcased!", $WORLDEXPO_ENEMY$);
        mes l("Collect the treasured crystals from treasure chests!");
        mes "";
        mes l("While %s's reasons to do so remain unclear, a request was made.", $WORLDEXPO_ENEMY$);
        mes l("Aurora will collect the crystals; And adventurers from the whole world shall look for them in the chests and return to Aurora!");
        mes "";
        mes l("This exposition has to be a success!");
        mesc l("Location: Treasure Chests, hidden inside dungeons."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Fishing") {
        mesc ".:: " + l("Catch the Golden Fish!") + " ::.", 2;
        mes "";
        mes l("A few times during the year, the \"golden wave\" migrates from the north pole to the south pole.");
        mes l("As a result, while fishing you may randomly get a golden fish.");
        mes "";
        mes l("These gold fishes multiply horribly and have no natural predator.");
        mes l("If left unchecked for long, they will eventually become the only specie on the sea.");
        mes l("Help controlling their population, and return those you fish to Aurora!");
        mes "";
        mes l("Catch the golden fish swarm!");
        mesc l("Location: Fishing spots in the water."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Mining") {
        mesc ".:: " + l("Miners Union Research Request!") + " ::.", 2;
        mes "";
        mes l("While mining bifs, a strange powder appeared.");
        mes l("The effects and uses - if any - are still unknown.");
        mes "";
        mes l("The Miners Union is trying to research this powder.");
        mes l("A reward is promised to those whom cooperate.");
        mes l("Collaborate by giving this powder to Aurora, before it vanishes!");
        mes "";
        mes l("Mysterious Powder expires after some time!");
        mesc l("Location: All bifs in the world."), 3;
        /////////////////////////////////////////////////////////////////////////
    } else if ($EVENT$ == "Candor") {
        mesc ".:: " + l("Candor Battle Season") + " ::.", 2;
        mes "";
        mes l("During this season, challenges to Crazyfefe will be free.");
        mes l("Special battle modes will also be available.");
        mes "";
        mes l("Will you accept his challenge?! Raise to the highest top score!");
        mesc l("Location: Candor B1F"), 3;
        /////////////////////////////////////////////////////////////////////////
    } else {
        mesc l("There's no help available for this event.");
        /////////////////////////////////////////////////////////////////////////
    }
    next;
    return;
}








function	script	Journalman	{
    npctalk3 any(
        l("Halinarzo Church makes a party every Sunday! Only true believers are invited!"),
        l("Beware the Terranite! Only @@ would be brave enough to challenge them!", ($MOST_HEROIC$ == "" ? "Andrei Sakar" : $MOST_HEROIC$)),
        l("Terrible manaquake hits the whole world, causing dramatic changes!"),
        l("Green Wars project says that @@ trees were planted by adventurers on the world!", format_number($TREE_PLANTED)),
        l("The Team For A Better PvP says that @@ players were killed in fair matches!", format_number($PLAYERS_KILLED)),
        l("The Alliance says that @@ monsters have been slain by players since 2019-05-24!", format_number($MONSTERS_KILLED)),
        l("All hail @@ and Andrei Sakar, heroes of the world!", $MOST_HEROIC$));
    mesn getarg(0);
    mesq l("Bonjour! I am @@, and I am from the Press! Read the latest news with me!", getarg(0));
    mes "";
    // Report any ongoing event
    if ($EVENT$ != "") {
        mesc l("It's @@ (day)!", $EVENT$);
        mesc l("Try talking to Soul Menhir or with any NPC on Tulimshar Centre (near Soul Menhir).");
    }

    if ($@GM_EVENT)
        mesc l("An event is happening at Aeros! Hurry up!");
    else if ($@MK_SCENE)
        mesc l("The Monster King is on the move!");

    switch (season()) {
        case SPRING:
            mesc l("It's spring! Two lovely NPCs at woodlands can be found...");
            break;
        case WINTER:
            mesc l("It's winter! An NPC in Nivalis Town is freezing...");
            break;
        case SUMMER:
            mesc l("It's summer! Ched is having his usual contest, but Luffyx in Hurnscald is up to no good!");
            break;
        case AUTUMN:
            mesc l("It's autumn! There is no special event during autumn, only special drops.");
            break;
        default:
            Exception(l("Invalid season: @@", season()), RB_DEFAULT|RB_SPEECH); break;
    }
    mes "";
    switch ($GAME_STORYLINE) {
        case 0:
            mesc l("The Monster King Army is occupying several towns! Brave players need to group and retake them!");
            break;
        case 1:
            mesc l("The Monster King Army left the cities! What will happen next? Anxiety grows!");
            break;
        case 2:
            .@def=100-(($MK_TEMPVAR+rand2(-1,1))/5);
            if ($@MK_AGGRO >= 300)
                .@st$=col(b(l("very mad")), 1);
            else if ($@MK_AGGRO >= 200)
                .@st$=col(b(l("very angry")), 6);
            else if ($@MK_AGGRO >= 100)
                .@st$=col(b(l("furious")), 7);
            if ($@MK_AGGRO >= 50)
                .@st$=col(b(l("angry")), 4);
            else
                .@st$=col(b(l("discontent")), 9);
            mesc l("The Monster King Army is attacking towns at random, but players already reduced their organization to @@ %%!", .@def);
            mesc l("With recent player activity, the Monster king is @@!", .@st$);
            break;
        case 3:
            mesc l("The Monster Army is in complete disarray, sieges are much less frequent.");
            mesc l("The mist over the Impregnable Fortress Peak finally lowered down, and it seems... The peaks are no more!");
            mesc l("The Monster King Lair is not in a impregnable mountain, but in a small island now!!");
            mesc l("Will adventurers reach it? Will the random attacks at towns cease??");
            break;
        case 4:
            mesc l("We must defeat the Monster King on his evil lair!");
            mesc l("Only then we may have peace!!");
            break;
        default:
            Exception(l("I do now know what this means: GS-@@-ICXN-@@", $GAME_STORYLINE, $MK_TEMPVAR), RB_DEFAULT|RB_SPEECH); break;
    }
    next;

    do {
        mes "";
        menuint
            l("Thanks for your help!"), 1,
            l("Event News"), 0,
            rif($GAME_STORYLINE >= 4, l("The Fifth Act")), 104,
            rif($GAME_STORYLINE >= 3, l("The Fourth Act")), 103,
            rif($GAME_STORYLINE >= 2, l("The Third Act")), 102,
            rif($GAME_STORYLINE >= 1, l("The Second Act")), 101,
            rif($GAME_STORYLINE >= 0, l("The First Act")), 100,
            l("Prologue"), 99,
            l("Eh, I have to go."), 1;
        mes "";
        if (@menuret > 1)
            GameNews(@menuret);
        if (@menuret == 0)
            EventHelp();
    } while (@menuret != 1);

    mesn getarg(0);
    mesq l("Good bye!");
    close;
    return;
}
