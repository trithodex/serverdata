// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Forge bullets
017-4,35,35,0	script	Pihro	NPC_PLAYER,{
    // cond_check(ItemID, ItemAmount)
    // Returns 1 if ID is zero
    function cond_check {
        if (getarg(0) == 0)
            return 1;
        else
            return (countitem(getarg(0)) >= getarg(1));
    }

    // craft_ammo( PrizeItem, Price, Lead Amount, {Elemental Item, Amount, {Elemental Item2, Amount2}} )
    function craft_ammo {
        .@prize=getarg(0);
        .@price=getarg(1);
        .@base1=LeadIngot;
        .@amon1=getarg(2);
        .@base2=getarg(3,0);
        .@amon2=getarg(4,0);
        .@base3=getarg(5,0);
        .@amon3=getarg(6,0);

		.@price=POL_AdjustPrice(.@price);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        if (.@amon2 > 0)
            mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        if (.@amon3 > 0)
            mesc l("@@/@@ @@", countitem(.@base3), .@amon3, getitemlink(.@base3));
        mesc l("@@/@@ GP", format_number(Zeny), .@price);
        next;

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            cond_check(.@base2, .@amon2) &&
            cond_check(.@base3, .@amon3) &&
            Zeny >= .@price) {
            inventoryplace .@prize, 220;
            delitem .@base1, .@amon1;
            if (.@base2 > 0)
                delitem .@base2, .@amon2;
            if (.@base3 > 0)
                delitem .@base3, .@amon3;
			POL_PlayerMoney(.@price);
            getitem .@prize, rand2(270,320);

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;
    }
    mesn;
    mesq l("Hello there! I make bullets for the weapons my friend Pyndragon makes.");
    next;

L_Craft:
    mesn;
    mesq l("Some bullets have Elemental Properties, by the way. Now, what will it be?");
    next;
    select
        l("Nothing, sorry."),
        l("Normal Bullet"),
        l("Sacred Bullet"),
        l("Explosive ARROW");

    switch (@menu) {
        case 2:
            // craft_ammo( PrizeItem, Price, Lead Amount, {Elemental Item, Amount, {Elemental Item2, Amount2}} )
            craft_ammo(Bullet, 200, 1);
            goto L_Craft;
        case 3:
            craft_ammo(SacredBullet, 200, 1, WhiteFur, 5);
            goto L_Craft;
        case 4:
            craft_ammo(ExplosiveArrow, 300, 1, SulfurPowder, 5, WoodenLog, 3);
            goto L_Craft;
    }
    close;


OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Monocle);
    setunitdata(.@npcId, UDT_HEADMIDDLE, WarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;
}
