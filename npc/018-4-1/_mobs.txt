// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-4-1: Secret Island Cave mobs
// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-4-1: sicave mobs
018-4-1,95,66,66,35	monster	Cave Snake	1035,15,9000,3000
018-4-1,95,84,66,18	monster	Golden Scorpion	1078,7,10000,5000
018-4-1,102,48,40,17	monster	Giant Mutated Bat	1044,2,100000,300000
018-4-1,142,64,27,44	monster	Black Mamba	1174,3,100000,50000
018-4-1,51,64,27,44	monster	Troll	1171,2,90000,90000
018-4-1,122,58,21,3	monster	Shadow Plant	1189,3,30000,30000
