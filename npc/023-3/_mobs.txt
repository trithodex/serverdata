// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 023-3: Ice Caves mobs
023-3,63,215,3,2	monster	Pollet	1219,2,30000,30000
023-3,41,214,16,4	monster	Cyan Butterfly	1172,2,30000,30000
023-3,39,217,16,4	monster	Iced Fluffy	1041,2,30000,30000
023-3,90,214,5,5	monster	Wolvern	1037,1,30000,30000
023-3,63,188,12,7	monster	Pollet	1219,2,30000,30000
023-3,120,54,80,16	monster	Assassin	1062,6,30000,30000
023-3,181,42,13,12	monster	Bluepar	1177,2,30000,30000
023-3,49,55,13,12	monster	Bluepar	1177,2,30000,30000
023-3,137,170,42,37	monster	Blue Slime	1087,11,30000,30000
023-3,163,96,36,33	monster	Moggun	1070,6,30000,30000
023-3,68,180,29,23	monster	Santa Slime	1096,4,30000,30000
023-3,67,90,21,21	monster	Slime Blast	1090,3,30000,30000
023-3,72,113,25,37	monster	White Slime	1094,4,30000,30000
023-3,51,134,17,25	monster	Azul Slime	1095,3,30000,30000
023-3,123,68,22,21	monster	Rudolph Slime	1086,2,30000,30000
023-3,122,201,31,13	monster	Rudolph Slime	1086,2,30000,30000
023-3,129,170,31,5	monster	Archant	1026,3,30000,30000
023-3,93,181,22,68	monster	Water Fairy	1184,2,30000,30000
023-3,93,57,22,66	monster	Nature Fairy	1186,2,30000,30000
023-3,117,127,91,101	monster	Ice Maggot	1012,32,30000,30000
023-3,105,157,91,86	monster	Cave Bat	1039,22,30000,30000
