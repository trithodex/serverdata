// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-6-1: Elenium Mines mobs
018-6-1,40,49,7,8	monster	Big Elenium Bif	1228,5,450000,45000
018-6-1,95,51,7,8	monster	Big Elenium Bif	1228,5,450000,45000
018-6-1,145,83,6,5	monster	Medium Elenium Bif	1227,5,400000,45000
018-6-1,62,86,6,5	monster	Medium Elenium Bif	1227,5,400000,45000
018-6-1,65,130,6,5	monster	Medium Elenium Bif	1227,5,400000,45000
018-6-1,125,158,6,5	monster	Small Elenium Bif	1226,4,300000,45000
018-6-1,129,183,6,5	monster	Small Elenium Bif	1226,4,300000,45000
018-6-1,34,181,5,5	monster	Small Elenium Bif	1226,4,300000,45000
018-6-1,80,172,54,25	monster	Black Slime	1178,16,30000,15000
018-6-1,89,167,54,25	monster	Dark Lizard	1051,8,30000,15000
018-6-1,96,103,56,29	monster	Black Scorpion	1074,17,30000,15000
018-6-1,101,105,54,25	monster	Mountain Snake	1123,9,30000,15000
018-6-1,70,45,44,25	monster	Wicked Mushroom	1176,14,30000,15000
018-6-1,68,45,31,21	monster	Archant	1026,7,30000,15000
018-6-1,39,51,8,6	monster	Mountain Snake	1123,1,30000,15000
018-6-1,92,53,8,6	monster	Mountain Snake	1123,1,30000,15000
