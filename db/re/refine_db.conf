//================= Hercules Database =====================================
//=       _   _                     _
//=      | | | |                   | |
//=      | |_| | ___ _ __ ___ _   _| | ___  ___
//=      |  _  |/ _ \ '__/ __| | | | |/ _ \/ __|
//=      | | | |  __/ | | (__| |_| | |  __/\__ \
//=      \_| |_/\___|_|  \___|\__,_|_|\___||___/
//================= License ===============================================
//= This file is part of Hercules.
//= http://herc.ws - http://github.com/HerculesWS/Hercules
//=
//= Copyright (C) 2015  Hercules Dev Team
//=
//= Hercules is free software: you can redistribute it and/or modify
//= it under the terms of the GNU General Public License as published by
//= the Free Software Foundation, either version 3 of the License, or
//= (at your option) any later version.
//=
//= This program is distributed in the hope that it will be useful,
//= but WITHOUT ANY WARRANTY; without even the implied warranty of
//= MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//= GNU General Public License for more details.
//=
//= You should have received a copy of the GNU General Public License
//= along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=========================================================================
//= Renewal Refine Database
//=========================================================================

/**************************************************************************
 ************* Entry structure ********************************************
 **************************************************************************
Armors/WeaponLevel1~4: {                                       // Specifies weapon level or armor type.
															   // - For armors, values of 100 add 1 armor defense.
															   // - For weapons, values of 100 add 1 ATK & MATK.
	StatsPerLevel: (int)                                       // This value is applied for ever level.
	RandomBonusStartLevel: (int)                               // This value specifies the start point for those levels that give a random bonus value (usually the first unsafe upgrade).
															   // - RandomBonusStartLevel is only applied for weapons, and not displayed client-side.
	RandomBonusValue: (int)                                    // A random number between 0 and (Random bonus start level - Upgrade level + 1) * this value is applied for all upgrades past.

	RefineryUISettings: (
		{
			Level: (int or array of int)                       // Holds either the individule refine level meant for this setting or an array defining a range
			                                                      of Low to Max level
			BlacksmithBlessing: (int) (optional)               // How many Blacksmith Blessing required for this range to be safe from breaking
			Announce: "(string)" (optional)                    // Sends an announcement server wide when a player reach this refine level using
			                                                      Refinery UI, this feature is only available starting from 2017-08-30 RagexeRE or
																  2017-09-06 Ragexe the field accepts the following values and it defaults to not announce
																  Success to set the announcement on item refine successful
																  Failure to set the announcement on item refine failure
																  Always to always announce it
			Items: {
				AegisName: {
					Type: "(string)"                           // The type to determine the chances used for this item, REFINE_CHANCE_TYPE_*
                                                                  constants are used in here
					Cost: (int) (optional)                     // Amount of zeny required
					FailureBehavior: "(string)" (optional)     // The expected behvaior on failure for this item, the following strings are used in here
																  Destroy (default) sets the item to be destroyed on failure
																  Keep keeps the item after failure
																  Downgrade downgrades the item by one level on failure
				}
			}
		}
	)

	Rates: {                                                   // Per level configuration of the refine rates.
		Lv1~20: {                                              // Lv1 ~ Lv20.
			NormalChance: (int)                                // (optional, defaults to 100) Chance of successful refine using normal ores (100 = 100%).
			EnrichedChance: (int)                              // (optional, defaults to 100 for weapons below refine level 20, otherwise 0.) Chance of successful refine using enriched ores (100 = 100%).
			EventNormalChance: (int)                           // (optional, defaults to 100) Chance of successful refine using normal ores (100 = 100%) during a refine event.
			EventEnrichedChance: (int)                         // (optional, defaults to 100 for weapons below refine level 10, otherwise 0.) Chance of successful refine using enriched ores (100 = 100%) during a refine event.
			Bonus: (int)                                       // (optional, defaults to 0) Bonus (Armor) for this level of refine.
		}
		// Note: Refine levels that use default values need not be listed. (Example: Lv1: { NormalChance: 100 Bonus: 0 })
	}
}

IMPORTANT: On TMW2, max refine level set by scripts is 10
**************************************************************************/

Armors: {
	StatsPerLevel: 0
	RandomBonusStartLevel: 0
	RandomBonusValue: 0
	Rates: {
		Lv1: {
			Bonus: 350
		}
		Lv2: {
			NormalChance: 93
			EnrichedChance: 99
			Bonus: 350
		}
		Lv3: {
			NormalChance: 86
			EnrichedChance: 96
			Bonus: 350
		}
		Lv4: {
			NormalChance: 79
			EnrichedChance: 93
			Bonus: 350
		}
		Lv5: {
			NormalChance: 72
			EnrichedChance: 90
			Bonus: 350
		}
		Lv6: {
			NormalChance: 65
			EnrichedChance: 80
			Bonus: 450
		}
		Lv7: {
			NormalChance: 58
			EnrichedChance: 70
			Bonus: 450
		}
		Lv8: {
			NormalChance: 51
			EnrichedChance: 60
			Bonus: 450
		}
		Lv9: {
			NormalChance: 44
			EnrichedChance: 50
			Bonus: 500
		}
		Lv10: {
			NormalChance: 37
			EnrichedChance: 40
			Bonus: 500
		}
        // Anything below this line was NOT MAINTENANED
		Lv11: {
			NormalChance: 30
			EnrichedChance: 30
			Bonus: 300
		}
		Lv12: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 300
		}
		Lv13: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 400
		}
		Lv14: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 400
		}
		Lv15: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 400
		}
		Lv16: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 400
		}
		Lv17: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 500
		}
		Lv18: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 500
		}
		Lv19: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 500
		}
		Lv20: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 500
		}
	}
    // This is junk
	RefineryUISettings: (
		{
			Level: [1, 20]
			Items: {
				Acorn: {
					Type: "REFINE_CHANCE_TYPE_NORMAL"
					Cost: 100000000
				}
			}
		},
	)
}

// Weapon level 1 is the standard tier for most weapons
WeaponLevel1: {
	StatsPerLevel: 800
	RandomBonusStartLevel: 6
	RandomBonusValue: 300
	Rates: {
		Lv2: {
			NormalChance: 93
			EnrichedChance: 99
		}
		Lv3: {
			NormalChance: 86
			EnrichedChance: 96
		}
		Lv4: {
			NormalChance: 79
			EnrichedChance: 93
		}
		Lv5: {
			NormalChance: 72
			EnrichedChance: 90
		}
		Lv6: {
			NormalChance: 65
			EnrichedChance: 80
		}
		Lv7: {
			NormalChance: 58
			EnrichedChance: 70
		}
		Lv8: {
			NormalChance: 51
			EnrichedChance: 60
		}
		Lv9: {
			NormalChance: 44
			EnrichedChance: 50
		}
		Lv10: {
			NormalChance: 37
			EnrichedChance: 40
		}
        // Anything below this line was NOT MAINTENANED
		Lv11: {
			NormalChance: 30
			EnrichedChance: 30
		}
		Lv12: {
			NormalChance: 0
			EnrichedChance: 0
		}
		Lv13: {
			NormalChance: 0
			EnrichedChance: 0
		}
		Lv14: {
			NormalChance: 0
			EnrichedChance: 0
		}
		Lv15: {
			NormalChance: 0
			EnrichedChance: 0
		}
		Lv16: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 300
		}
		Lv17: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 300
		}
		Lv18: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 300
		}
		Lv19: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 300
		}
		Lv20: {
			NormalChance: 0
			EnrichedChance: 0
			Bonus: 300
		}
	}
    // This is junk
	RefineryUISettings: (
		{
			Level: [1, 20]
			Items: {
				Acorn: {
					Type: "REFINE_CHANCE_TYPE_NORMAL"
					Cost: 100000000
				}
			}
		},
	)
}



// XXX WARNING XXX:
//     Unused     :
// XXX WARNING XXX:
WeaponLevel2: {
	RefineryUISettings: (
		{
			Level: [1, 20]
			Items: {
				Acorn: {
					Type: "REFINE_CHANCE_TYPE_NORMAL"
					Cost: 100000000
				}
			}
		},
	)
	StatsPerLevel: 300
	RandomBonusStartLevel: 7
	RandomBonusValue: 500
	Rates: {
		Lv7: {
			NormalChance: 72
			EnrichedChance: 90
		}
		Lv8: {
			NormalChance: 58
			EnrichedChance: 70
		}
		Lv9: {
			NormalChance: 20
			EnrichedChance: 40
		}
		Lv10: {
			NormalChance: 19
			EnrichedChance: 30
		}
		Lv11: {
			NormalChance: 18
		}
		Lv12: {
			NormalChance: 18
		}
		Lv13: {
			NormalChance: 18
		}
		Lv14: {
			NormalChance: 18
		}
		Lv15: {
			NormalChance:18
		}
		Lv16: {
			NormalChance: 17
			Bonus: 600
		}
		Lv17: {
			NormalChance: 17
			Bonus: 600
		}
		Lv18: {
			NormalChance: 17
			Bonus: 600
		}
		Lv19: {
			NormalChance: 15
			Bonus: 600
		}
		Lv20: {
			NormalChance: 15
			Bonus: 600
		}
	}
}
WeaponLevel3: {
	RefineryUISettings: (
		{
			Level: [1, 20]
			Items: {
				Acorn: {
					Type: "REFINE_CHANCE_TYPE_NORMAL"
					Cost: 100000000
				}
			}
		},
	)
	StatsPerLevel: 500
	RandomBonusStartLevel: 6
	RandomBonusValue: 800
	Rates: {
		Lv6: {
			NormalChance: 72
			EnrichedChance: 90
		}
		Lv7: {
			NormalChance: 65
			EnrichedChance: 80
		}
		Lv8: {
			NormalChance: 20
			EnrichedChance: 40
		}
		Lv9: {
			NormalChance: 20
			EnrichedChance: 40
		}
		Lv10: {
			NormalChance: 19
			EnrichedChance: 30
		}
		Lv11: {
			NormalChance: 18
		}
		Lv12: {
			NormalChance: 18
		}
		Lv13: {
			NormalChance: 18
		}
		Lv14: {
			NormalChance: 18
		}
		Lv15: {
			NormalChance: 18
		}
		Lv16: {
			NormalChance: 17
			Bonus: 900
		}
		Lv17: {
			NormalChance: 17
			Bonus: 900
		}
		Lv18: {
			NormalChance: 17
			Bonus: 900
		}
		Lv19: {
			NormalChance: 15
			Bonus: 900
		}
		Lv20: {
			NormalChance: 15
			Bonus: 900
		}
	}
}
WeaponLevel4: {
	RefineryUISettings: (
		{
			Level: [1, 20]
			Items: {
				Acorn: {
					Type: "REFINE_CHANCE_TYPE_NORMAL"
					Cost: 100000000
				}
			}
		},
	)
	StatsPerLevel: 700
	RandomBonusStartLevel: 5
	RandomBonusValue: 1400
	Rates: {
		Lv5: {
			NormalChance: 72
			EnrichedChance: 90
		}
		Lv6: {
			NormalChance: 58
			EnrichedChance: 70
		}
		Lv7: {
			NormalChance: 58
			EnrichedChance: 70
		}
		Lv8: {
			NormalChance: 20
			EnrichedChance: 40
		}
		Lv9: {
			NormalChance: 20
			EnrichedChance: 40
		}
		Lv10: {
			NormalChance: 9
			EnrichedChance: 20
		}
		Lv11: {
			NormalChance: 8
		}
		Lv12: {
			NormalChance: 8
		}
		Lv13: {
			NormalChance: 8
		}
		Lv14: {
			NormalChance: 8
		}
		Lv15: {
			NormalChance: 7
		}
		Lv16: {
			NormalChance: 7
			Bonus: 1200
		}
		Lv17: {
			NormalChance: 7
			Bonus: 1200
		}
		Lv18: {
			NormalChance: 7
			Bonus: 1200
		}
		Lv19: {
			NormalChance: 5
			Bonus: 1200
		}
		Lv20: {
			NormalChance: 5
			Bonus: 1200
		}
	}
}
